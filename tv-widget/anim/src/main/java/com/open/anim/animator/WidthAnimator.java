package com.open.anim.animator;

import android.view.View;

/**
 * 宽度动画
 */
public class WidthAnimator extends ObjectNameAnimator {

    public WidthAnimator(View view, float... items) {
        super(view, "width", items);
    }

    public WidthAnimator(float... items) {
        super("width", items);
    }

}
