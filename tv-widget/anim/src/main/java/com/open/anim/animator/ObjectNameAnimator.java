package com.open.anim.animator;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.animation.TypeEvaluator;
import android.animation.ValueAnimator;
import android.view.View;

/**
 * 属性动画封装
 */
public class ObjectNameAnimator extends BaseAnimator {

    ValueAnimator mAnimator;

    public ObjectNameAnimator(final String name, final float... items) {
        this(null, name, items);
    }

    public ObjectNameAnimator(final View view, final String name, final float... items) {
        setTarget(view);
        mAnimator = ObjectAnimator.ofFloat(getTarget(), name, items);
    }

    @Override
    public void prepareAnimSet() {
        getAnimatorSet().play(
                mAnimator
        );
    }

    @Override
    public BaseAnimator setTypeEvaluator(TypeEvaluator value) {
        mAnimator.setEvaluator(value);
        return super.setTypeEvaluator(value);
    }

    public Animator getAnimator() {
        return mAnimator;
    }

    /**
     * 添加更新ValueAnim的回调
     *
     * @param cb ValueAnimator.AnimatorUpdateListener
     */
    public void addUpdateListener(ValueAnimator.AnimatorUpdateListener cb) {
        if (null != mAnimator) {
            mAnimator.addUpdateListener(cb);
        }
    }

}
