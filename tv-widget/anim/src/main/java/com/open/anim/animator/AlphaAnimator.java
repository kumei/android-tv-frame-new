package com.open.anim.animator;

import android.view.View;

/**
 * 透明动画效果
 */
public class AlphaAnimator extends ObjectNameAnimator {

    public AlphaAnimator() {
        this(0.0f, 1.0f);
    }

    public AlphaAnimator(float... items) {
        super("alpha", items);
    }

    public AlphaAnimator(View view, float... items) {
        super(view,"alpha", items);
    }

}
