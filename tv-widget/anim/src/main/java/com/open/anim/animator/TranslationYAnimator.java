package com.open.anim.animator;

import android.view.View;

/**
 * 移动动画效果
 */
public class TranslationYAnimator extends ObjectNameAnimator {

    public TranslationYAnimator() {
        this(0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
    }

    public TranslationYAnimator(float... items) {
        super("translationY", items);
    }

    public TranslationYAnimator(View view, float... items) {
        super(view, "translationY", items);
    }

}
