package com.open.anim.animator;

import android.graphics.Rect;
import android.view.View;

import java.lang.ref.WeakReference;

/**
 * 可以继承这个，完成自己需要的东西.
 */
public class TargetView implements ITargetView {

    WeakReference<Object> mTarget;

    public TargetView(View view) {
        mTarget = (view == null ? null : new WeakReference<Object>(view));
    }

    public void setWidth(float value) {
        View view = getView();
        if (null != view) {
            view.getLayoutParams().width = (int) value;
            view.requestLayout();
        }
    }

    public void setHeight(float value) {
        View view = getView();
        if (null != view) {
            view.getLayoutParams().height = (int) value;
            view.requestLayout();
        }
    }

    public void setRotation(float value) {
        View view = getView();
        if (null != view) {
            view.setRotation(value);
        }
    }

    public void setRotationX(float value) {
        View view = getView();
        if (null != view) {
            view.setRotationX(value);
        }
    }

    public void setRotationY(float value) {
        View view = getView();
        if (null != view) {
            view.setRotationY(value);
        }
    }

    public void setTranslationX(float value) {
        View view = getView();
        if (null != view) {
            view.setTranslationX(value);
        }
    }

    public void setTranslationY(float value) {
        View view = getView();
        if (null != view) {
            view.setTranslationY(value);
        }
    }

    public void setScaleX(float value) {
        View view = getView();
        if (null != view) {
            view.setScaleX(value);
        }
    }

    public void setScaleY(float value) {
        View view = getView();
        if (null != view) {
            view.setScaleY(value);
        }
    }

    public void setAlpha(float value) {
        View view = getView();
        if (null != view) {
            view.setAlpha(value);
        }
    }

    public void setOffsetLeftAndRight(float value) {
        View view = getView();
        if (null != view) {
            view.offsetLeftAndRight((int) value);
        }
    }

    /**
     * 上下左右均可设置，可以用于移动边框.
     */
    public void setLeftTopRightBottom(Rect rect) {
        View view = getView();
        if (null != view) {
//            view.setLeftTopRightBottom(rect.left, rect.top, rect.right, rect.bottom);
        }
    }

    @Override
    public View getView() {
        return null == mTarget ? null : (View) mTarget.get();
    }

}
