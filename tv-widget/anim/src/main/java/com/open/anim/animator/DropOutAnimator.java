package com.open.anim.animator;

import android.animation.Animator;
import android.animation.ObjectAnimator;

import com.open.anim.evaluator.BounceEaseOut;

public class DropOutAnimator extends BaseAnimator {

    @Override
    public void prepareAnimSet() {
        Animator animator = ObjectAnimator.ofFloat(getTarget(), "translationY", -100, 0);
        ((ObjectAnimator) animator).setEvaluator(new BounceEaseOut(500));
        getAnimatorSet().play(
                animator
        );
    }

}
