package com.open.anim.animator;

import android.view.View;

/**
 * 移动动画效果
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 19-03-15
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public class MoveYAnimator extends ObjectNameAnimator {

    public MoveYAnimator() {
        this(0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
    }

    public MoveYAnimator(float... items) {
        super("translationY", items);
    }

    public MoveYAnimator(View view, float... items) {
        super(view, "translationY", items);
    }

}
