package com.open.anim;

/**
 * 生命周期回调接口
 * TODO: 后续开发生命周期关联
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 19-03-15
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public interface LifecycleListener {

    /**
     * Callback for when {@link android.app.Fragment#onStart()}} or {@link
     * android.app.Activity#onStart()} is called.
     */
    void onStart();

    /**
     * Callback for when {@link android.app.Fragment#onStop()}} or {@link
     * android.app.Activity#onStop()}} is called.
     */
    void onStop();

    /**
     * Callback for when {@link android.app.Fragment#onDestroy()}} or {@link
     * android.app.Activity#onDestroy()} is called.
     */
    void onDestroy();

}
