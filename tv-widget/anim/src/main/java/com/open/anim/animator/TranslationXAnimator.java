package com.open.anim.animator;

import android.view.View;

/**
 * 移动动画效果
 */
public class TranslationXAnimator extends ObjectNameAnimator {

    public TranslationXAnimator() {
        this(0, 25, -25, 25, -25, 15, -15, 6, -6, 0);
    }

    public TranslationXAnimator(View view, float... items) {
        super(view, "translationX", items);
    }

    public TranslationXAnimator(float... items) {
        super("translationX", items);
    }

}
