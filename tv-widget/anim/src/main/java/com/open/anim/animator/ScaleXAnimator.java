package com.open.anim.animator;

import android.view.View;

/**
 * 缩放动画效果
 */
public class ScaleXAnimator extends ObjectNameAnimator {

    public ScaleXAnimator() {
        this(0.0f, 1.0f);
    }

    public ScaleXAnimator(float... items) {
        super("scaleX", items);
    }

    public ScaleXAnimator(View view,  float... items) {
        super(view,"scaleX", items);
    }

}
