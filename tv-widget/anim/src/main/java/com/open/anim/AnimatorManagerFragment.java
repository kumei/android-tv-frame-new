package com.open.anim;

import android.app.Activity;
import android.app.Fragment;
import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.LifecycleOwner;
import android.arch.lifecycle.LifecycleRegistry;
import android.support.annotation.NonNull;

public class AnimatorManagerFragment extends Fragment implements LifecycleOwner {

    private LifecycleRegistry mLifecycleRegistry;

    public AnimatorManagerFragment() {
        mLifecycleRegistry = new LifecycleRegistry(this);
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return mLifecycleRegistry;
    }

    @Override
    public void onStart() {
        super.onStart();
        dispatch(Lifecycle.Event.ON_START);
    }

    @Override
    public void onStop() {
        super.onStop();
        dispatch(Lifecycle.Event.ON_STOP);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dispatch(Lifecycle.Event.ON_DESTROY);
    }

    /**
     * 发送生命周期的刷新信号.
     */
    private void dispatch(Lifecycle.Event event) {
        Activity activity = getActivity();
        if (null != activity && !activity.isFinishing()) {
            ((LifecycleRegistry) getLifecycle()).handleLifecycleEvent(event);
        }
    }

}
