package com.open.anim.animator;

import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.RectEvaluator;
import android.annotation.TargetApi;
import android.graphics.Rect;
import android.os.Build;
import android.util.Property;
import android.view.View;

/**
 * 移动Left, top, right, bottom.
 */
@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class LTRBAnimator extends ObjectNameAnimator {

    public static final RectEvaluator RECT_EVALUATOR = new RectEvaluator(new Rect());
    public static final Property<View, Rect> LTRB =
            new Property<View, Rect>(Rect.class, "leftTopRightBottom") {

                private Rect mTmpRect = new Rect();

                @Override
                public void set(View v, Rect ltrb) {
//                     v.setLeftTopRightBottom(ltrb.left, ltrb.top, ltrb.right, ltrb.bottom);
                }

                @Override
                public Rect get(View v) {
                    mTmpRect.set(v.getLeft(), v.getTop(), v.getRight(), v.getBottom());
                    return mTmpRect;
                }
            };

    public LTRBAnimator(String name, float... items) {
        super(name, items);
    }

    public LTRBAnimator(View view, String name, float... items) {
        super(view, name, items);
//        mAnimator = ObjectAnimator.ofFloat(getTarget(), name, items);
        // 参考文章: https://wiki.jikexueyuan.com/project/android-animation/8.html
        Rect fromViewRect = new Rect();
        Rect toViewRect = new Rect();
        PropertyValuesHolder propertyValuesHolder = PropertyValuesHolder.ofObject(LTRB, RECT_EVALUATOR,
                fromViewRect, toViewRect);
        mAnimator = ObjectAnimator.ofPropertyValuesHolder(
                getTarget(), propertyValuesHolder);
    }

    /*
    参考谷歌的文档:http://developer.android.com/reference/android/animation/PropertyValuesHolder.html
    public static PropertyValuesHolder ofFloat(String propertyName, float... values)
    public static PropertyValuesHolder ofInt(String propertyName, int... values)
    public static PropertyValuesHolder ofObject(String propertyName, TypeEvaluator evaluator,Object... values)
    public static PropertyValuesHolder ofKeyframe(String propertyName, Keyframe... values)
     */

}
