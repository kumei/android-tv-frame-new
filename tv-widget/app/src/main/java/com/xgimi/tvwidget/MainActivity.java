package com.xgimi.tvwidget;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import com.open.anim.OpenAnim;
import com.open.anim.animator.TranslationXAnimator;
import com.open.anim.animator.TranslationYAnimator;

public class MainActivity extends FragmentActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        OpenAnim.with(this)
                .into(findViewById(R.id.button))
                .together(
                new TranslationXAnimator(500),
                new TranslationYAnimator(100))
                .duration(3000)
                // .repeatCount(ValueAnimator.INFINITE)
                .repeatCount(3)
                .start();
    }

}
