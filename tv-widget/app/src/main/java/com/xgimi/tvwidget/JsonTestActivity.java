package com.xgimi.tvwidget;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.open.common.json.GsonUtils;
import com.xgimi.tvwidget.mode.FactoryGsonMode;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class JsonTestActivity extends Activity {

    TextView mJsonTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_json_load);
        mJsonTv = findViewById(R.id.test_tv);
        FactoryGsonMode factoryGsonMode = GsonUtils.gsonToBean(readTestJson(this, R.raw.new_factory).toString(), FactoryGsonMode.class);
        Log.d("hailong.qiu", "factoryGsonMode:" + factoryGsonMode);
        StringBuilder sb = new StringBuilder();
        for (FactoryGsonMode.Page page : factoryGsonMode.getPages()) {
            sb.append(page.getPageTitle());
            sb.append("\n");
            for (FactoryGsonMode.Page.Item item : page.getItems()) {
                sb.append("\n");
                sb.append(item);
                sb.append("\n");
            }
            sb.append("\n");
        }
        mJsonTv.setText(sb.toString());
    }

    public static StringBuilder readTestJson(Context context, int id) {
        StringBuilder sb = new StringBuilder();
        try {
            InputStream is = context.getResources().openRawResource(id);
            InputStreamReader in = new InputStreamReader(is);
            BufferedReader bufferedReader = new BufferedReader(in);
            String line = null;
            while (null != (line = bufferedReader.readLine())) {
                sb.append(line);
            }
            bufferedReader.close();
            in.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb;
    }

}
