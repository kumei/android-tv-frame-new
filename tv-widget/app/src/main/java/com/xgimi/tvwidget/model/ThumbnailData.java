package com.xgimi.tvwidget.model;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.util.Log;

import com.open.common.joor.Reflect;

import java.sql.Ref;

public class ThumbnailData {

    public final Bitmap thumbnail;

    public ThumbnailData() {
        thumbnail = null;
    }

    public ThumbnailData(Object snapshot) {
        // thumbnail = null;
        if (null != snapshot) {
            Log.d("com.xgimi.tvwidget", "ThumbnailData getSnapshot " + snapshot);
            Object snapshotBitmap = Reflect.on(snapshot).call("getSnapshot").get();
            Log.d("com.xgimi.tvwidget", "ThumbnailData getSnapshot bitmap:" + snapshotBitmap);
            thumbnail = Reflect.on(Bitmap.class).call("createHardwareBitmap", snapshotBitmap).get();
        } else {
            thumbnail = null;
        }
        // thumbnail = Bitmap.createHardwareBitmap(snapshot.getSnapshot());
        // insets = new Rect(snapshot.getContentInsets());
        // orientation = snapshot.getOrientation();
        // reducedResolution = snapshot.isReducedResolution();
        // scale = snapshot.getScale();
        // isRealSnapshot = snapshot.isRealSnapshot();
        // isTranslucent = snapshot.isTranslucent();
        // windowingMode = snapshot.getWindowingMode();
        // systemUiVisibility = snapshot.getSystemUiVisibility();
    }

}
