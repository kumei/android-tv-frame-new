package com.xgimi.tvwidget.mode;

import java.util.List;

/**
 * 工厂菜单JSON数据
 */
public class FactoryGsonMode {
    int versionCode;
    long updateTime;
    List<Page> pages;

    public int getVersionCode() {
        return versionCode;
    }

    public void setVersionCode(int versionCode) {
        this.versionCode = versionCode;
    }

    public long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(long updateTime) {
        this.updateTime = updateTime;
    }

    public List<Page> getPages() {
        return pages;
    }

    public void setPages(List<Page> pages) {
        this.pages = pages;
    }

    @Override
    public String toString() {
        return "FactoryGsonMode{" +
                "versionCode=" + versionCode +
                ", updateTime=" + updateTime +
                ", pages=" + pages +
                '}';
    }

    public class Page {
        String pageTitle;
        List<Item> items;

        public String getPageTitle() {
            return pageTitle;
        }

        public void setPageTitle(String pageTitle) {
            this.pageTitle = pageTitle;
        }

        public List<Item> getItems() {
            return items;
        }

        public void setItems(List<Item> items) {
            this.items = items;
        }

        @Override
        public String toString() {
            return "Page{" +
                    "pageTitle='" + pageTitle + '\'' +
                    ", items=" + items +
                    '}';
        }

        public class Item {
            String itemName;
            String itemTag;
            String titleTvColor;
            int itemType; // 0 正常类型 1
            String action;
            int actionType;

            public String getItemName() {
                return itemName;
            }

            public void setItemName(String itemName) {
                this.itemName = itemName;
            }

            public String getItemTag() {
                return itemTag;
            }

            public void setItemTag(String itemTag) {
                this.itemTag = itemTag;
            }

            public String getTitleTvColor() {
                return titleTvColor;
            }

            public void setTitleTvColor(String titleTvColor) {
                this.titleTvColor = titleTvColor;
            }

            public int getItemType() {
                return itemType;
            }

            public void setItemType(int itemType) {
                this.itemType = itemType;
            }

            public String getAction() {
                return action;
            }

            public void setAction(String action) {
                this.action = action;
            }

            public int getActionType() {
                return actionType;
            }

            public void setActionType(int actionType) {
                this.actionType = actionType;
            }

            @Override
            public String toString() {
                return "Item{" +
                        "itemName='" + itemName + '\'' +
                        ", itemTag='" + itemTag + '\'' +
                        ", titleTvColor='" + titleTvColor + '\'' +
                        ", itemType=" + itemType +
                        ", action='" + action + '\'' +
                        ", actionType=" + actionType +
                        '}';
            }

        }

    }

}
