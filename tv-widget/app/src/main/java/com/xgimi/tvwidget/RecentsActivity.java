package com.xgimi.tvwidget;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.RemoteException;
import android.support.annotation.LongDef;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.open.common.joor.Reflect;
import com.xgimi.tvwidget.model.Task;
import com.xgimi.tvwidget.model.ThumbnailData;
import com.xgimi.tvwidget.view.TaskViewThumbnail;

import java.util.List;
import java.util.Random;

public class RecentsActivity extends Activity {

    TaskViewThumbnail mTaskViewThumbnail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recents);
        mTaskViewThumbnail = findViewById(R.id.task_view_thumbnail);
        findViewById(android.R.id.content).postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void run() {
                testGetAllTaskInfo();
            }
        }, 1000);
        //
        mDummyIcon = Bitmap.createBitmap(1, 1, Bitmap.Config.ARGB_8888);
        mDummyIcon.eraseColor(0xFF999999);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void testGetAllTaskInfo() {
        ActivityManager am = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        final PackageManager pm = getPackageManager();
        if (null != am) {
            final List<ActivityManager.RecentTaskInfo> recentTasks = am
                    .getRecentTasks(Integer.MAX_VALUE, 0x0002);
            final List<ActivityManager.RunningTaskInfo> taskInfos = am.getRunningTasks(Integer.MAX_VALUE);
            for (ActivityManager.RecentTaskInfo taskInfo : recentTasks) {
                Intent intent = new Intent(taskInfo.baseIntent);
                intent.setFlags((intent.getFlags() & ~Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED)
                        | Intent.FLAG_ACTIVITY_NEW_TASK);
                final ResolveInfo resolveInfo = pm.resolveActivity(intent, 0);
                if (resolveInfo != null) {
                    final ActivityInfo activityInfo = resolveInfo.activityInfo;
                    final String title = activityInfo.loadLabel(pm).toString();
                    Drawable icon = activityInfo.loadIcon(pm);

                    ComponentName componentName = taskInfos.get(0).topActivity;
                    String packName = componentName.getPackageName();
                    String className = componentName.getClassName();
                    Log.d("com.xgimi.tvwidget", "taskId:" + taskInfo.persistentId + " title:" + title + " icon:" + icon +
                            " packName:" + packName + " className:" + className);
                    mTaskViewThumbnail.setBackground(icon);
                }
                if (recentTasks.indexOf(taskInfo) == recentTasks.size() - 1) {
                    // 获取预览图
                    Bitmap bitmap = getTaskThumbnailsBitmap(am, taskInfo.persistentId);
                    mTaskViewThumbnail.setThumbnail(bitmap);
                }
            }
        }
    }

    boolean isFristGet = true;
    Bitmap mDummyIcon;

    private void killBackgroundProcesses() {
        // am.killBackgroundProcesses(pkgList[j]);
    }

    private Bitmap getTaskThumbnailsBitmap(ActivityManager am, Integer id) {
        try {
            // Object thumbnails = Reflect.on(am)
            //                        .call("getTaskThumbnail", id)
            //                        .get();
            // Log.d("com.xgimi.tvwidget", "bitmap:" + thumbnails);
            // Object mainThumbnail = Reflect.on(thumbnails).field("mainThumbnail").get();
            // Log.d("com.xgimi.tvwidget", "mainThumbnail:" + mainThumbnail);
            // return Reflect.on(thumbnails).field("mainThumbnail").get();
            // return (Bitmap) thumbnails;
            ThumbnailData thumbnailData =  getTaskThumbnail(id, true);
            if (null != thumbnailData && thumbnailData.thumbnail != null) {
                return  thumbnailData.thumbnail;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        // getTaskThumbnail(id, true);
        return null; //getTaskThumbnail(id, true).thumbnail;
    }

    ThumbnailData getTaskThumbnail(int taskId, boolean reducedResolution) {
        Object snapshot = null; // ActivityManager.TaskSnapshot
        try {
            Object service = Reflect.on("android.app.ActivityManager").call("getService").get();
            Log.d("com.xgimi.tvwidget", "getTaskThumbnail service " + service + " taskId:" + taskId);
            snapshot = Reflect.on(service).call("getTaskSnapshot", taskId, reducedResolution).get();
            Log.d("com.xgimi.tvwidget", "getTaskThumbnail snapshot " + snapshot);
            // snapshot = ActivityManager.getService().getTaskSnapshot(taskId, reducedResolution);
        } catch (Exception e) {
            Log.w("com.xgimi.tvwidget", "Failed to retrieve task snapshot", e);
        }
        if (snapshot != null) {
            return new ThumbnailData(snapshot);
        } else {
            return new ThumbnailData();
        }
    }

}
