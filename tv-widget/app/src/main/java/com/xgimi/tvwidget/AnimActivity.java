package com.xgimi.tvwidget;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.open.anim.AnimatorManager;
import com.open.anim.OpenAnim;
import com.open.anim.animator.AddAnimator;
import com.open.anim.animator.AlphaAnimator;
import com.open.anim.animator.BaseAnimator;
import com.open.anim.animator.ObjectNameAnimator;
import com.open.anim.animator.RotationAnimator;
import com.open.anim.animator.ScaleXAnimator;
import com.open.anim.animator.ScaleYAnimator;
import com.open.anim.animator.TranslationXAnimator;
import com.open.anim.animator.TranslationYAnimator;
import com.open.anim.evaluator.BounceEaseOut;

public class AnimActivity extends Activity implements View.OnFocusChangeListener {


    Button mTestBtn;
    Button mTestBtn2;
    ImageView mShouZhi;
    ImageView mDuiGou;
    View mScaleRiplleView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTestBtn = findViewById(R.id.button);
        mTestBtn2 = findViewById(R.id.button2);
        mShouZhi = findViewById(R.id.showzhi_iv);
        mDuiGou = findViewById(R.id.duigoud_iv);
        mScaleRiplleView = findViewById(R.id.scale_ripple_view);
        mTestBtn.setOnFocusChangeListener(this);
        mTestBtn2.setOnFocusChangeListener(this);
        testAnim();
        // testAnim();
        // testAnim2();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

    @SuppressLint("ResourceType")
    private void testAnim() {
        // OpenAnim.with(this)
        //         .into(mTestBtn)
        //         .together(
        //                 new TranslationXAnimator(500),
        //                 new TranslationYAnimator(100),
        //                 // new RotationXAnimator(0, 360),
        //                 // new RotationYAnimator(0, 360),
        //                 new RotationAnimator(0, 360))
        //         .duration(3000)
        //         // .interpolate(new LinearInterpolator())
        //         // .repeatCount(3)
        //         .repeatCount(ValueAnimator.INFINITE)
        //         .start();

        AddAnimator addAnimator = new AddAnimator();
        // addAnimator.setTypeEvaluator(new BounceEaseOut(500));
        addAnimator.playTogether(
                new ObjectNameAnimator(mScaleRiplleView, "newWidth", 0, 40, 0),
                new ObjectNameAnimator(mScaleRiplleView, "newHeight", 0, 10, 0));
        addAnimator.setTypeEvaluator(new BounceEaseOut(500));
        // addAnimator.setStartDelay(10000);
        addAnimator.setDuration(3000);

        RotationAnimator rotationAnimator = new RotationAnimator(mShouZhi, 0, 15, 0);
        // rotationAnimator.setTypeEvaluator(new BounceEaseOut(500));
        rotationAnimator.setDuration(1300);

        mShouZhi.setPivotX(300);
        mShouZhi.setPivotY(100);
        OpenAnim.with(this)
                // .into(mTestBtn2)
                // .together(R.anim.test_anim)
                .together(rotationAnimator, addAnimator)
                // .play(addAnimator).with(rotationAnimator)
                // .duration(1500)
                .interpolate(new AccelerateDecelerateInterpolator())
                .repeatCount(ValueAnimator.INFINITE)
                // .typeEvaluator(new BounceEaseOut(500))
                // .repeatCount(3)
                // .startDelay(1000)
                .start();

        // OpenAnim.with(this)
        //         .together(new TranslationYAnimator(mDuiGou,0, 50, 0))
        //         .duration(2000)
        //         .repeatCount(ValueAnimator.INFINITE)
        //         .start();

        // Animator animator = AnimatorInflater.loadAnimator(this, R.anim.test_anim);
        // animator.setTarget(mTestBtn2);
        // animator.start();
        // if (animator instanceof AnimatorSet) {
        //     AnimatorSet animatorSet = (AnimatorSet) animator;
        //     Log.d("hailong.qiu", "getChildAnimations:" + animatorSet.getChildAnimations());
        // }
    }

    /**
     * together: 同时一起执行
     * sequentially: 顺序执行
     */
    private void testAnim1() {
        OpenAnim.with(this).together(
                new TranslationXAnimator(mTestBtn2, 500),
                new TranslationYAnimator(mTestBtn, 100))
                .duration(3000)
                .repeatCount(ValueAnimator.INFINITE)
                .start();
    }

    private void testAnim2() {
        // 按钮1的动画集合.
        BaseAnimator testAnimator = OpenAnim.with(this)
                                            .into(mTestBtn)
                                            .sequentially(new TranslationXAnimator(), new AlphaAnimator())
                                            .getAnimator();
        // 按钮2的动画集合.
        BaseAnimator test2Animator = OpenAnim.with(this)
                                             .into(mTestBtn2)
                                             .sequentially(new TranslationYAnimator(), new AlphaAnimator())
                                             .getAnimator();
        OpenAnim.with(this)
                // .sequentially(testAnimator, test2Animator)
                .together(testAnimator, test2Animator)
                .duration(3000)
                .repeatCount(3) // BUG:多个动画组合的时候，重复次数无效.
                .start();
    }

    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        // AnimatorManager am = OpenAnim.with(this).into(v).together(
        //         new ScaleXAnimator(hasFocus ? 1.0f : 1.5f, hasFocus ? 1.5f : 1.0f),
        //         new ScaleYAnimator(hasFocus ? 1.0f : 1.5f, hasFocus ? 1.5f : 1.0f))
        //                              .duration(300)
        //                              .start();
        // am.onDestroy();
        // if (hasFocus) {
        //     OpenAnim.into(v).duration(1300).play(new MoveYAnimator(-30)).start();
        // } else {
        //     OpenAnim.into(v).duration(1300).play(new MoveYAnimator(0)).start();
        // }
        // OpenAnim.into(v).duration(1300).play(new MoveYAnimator(hasFocus ? -30 : 0)).start();
    }

}
