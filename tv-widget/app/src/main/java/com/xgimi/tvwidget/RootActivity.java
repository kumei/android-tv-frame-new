package com.xgimi.tvwidget;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RootActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root);
        ButterKnife.bind(this);
    }

    @OnClick({R.id.anim_btn, R.id.ui_btn, R.id.json_btn, R.id.recents_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.anim_btn:
                startActivity(new Intent(this, AnimActivity.class));
                break;
            case R.id.ui_btn:
                break;
            case R.id.json_btn:
                startActivity(new Intent(this, JsonTestActivity.class));
                break;
            case R.id.recents_btn:
                startActivity(new Intent(this, RecentsActivity.class));
                break;
        }
    }

}
