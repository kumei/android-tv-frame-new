package com.xgimi.tvwidget.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.AttributeSet;
import android.view.View;

import com.xgimi.tvwidget.model.Task;

public class TaskViewThumbnail extends View {

    BitmapShader mBitmapShader;
    Paint mDrawPaint = new Paint();
    RectF mBitmapRect = new RectF();
    Matrix mScaleMatrix = new Matrix();
    // Visibility optimization, if the thumbnail height is less than the height of the header
    // bar for the task view, then just mark this thumbnail view as invisible
    boolean mInvisible = false;
    RectF mLayoutRect = new RectF();
    float mDimAlpha = 0.5f;
    float mThumbnailAlpha;
    LightingColorFilter mLightingColorFilter = new LightingColorFilter(0xffffffff, 0);

    public TaskViewThumbnail(Context context) {
        this(context, null);
    }

    public TaskViewThumbnail(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TaskViewThumbnail(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        // mConfig = RecentsConfiguration.getInstance();
        mDrawPaint.setColorFilter(mLightingColorFilter);
        mDrawPaint.setFilterBitmap(true);
        mDrawPaint.setAntiAlias(true);
    }

    public void rebindToTask(Task t) {
        if (t.thumbnail != null) {
            setThumbnail(t.thumbnail);
        } else {
            setThumbnail(null);
        }
    }

    Bitmap mBitmap;

    public void setThumbnail(Bitmap bm) {
        mBitmap = bm;
        if (bm != null) {
            mBitmapShader = new BitmapShader(bm, Shader.TileMode.CLAMP,
                    Shader.TileMode.CLAMP);
            mDrawPaint.setShader(mBitmapShader);
            mBitmapRect.set(0, 0, bm.getWidth(), bm.getHeight());
            updateThumbnailScale();
        } else {
            mBitmapShader = null;
            mDrawPaint.setShader(null);
        }
        updateThumbnailPaintFilter();
        invalidate();
    }

    void updateThumbnailScale() {
        if (mBitmapShader != null) {
            mScaleMatrix.setRectToRect(mBitmapRect, mLayoutRect, Matrix.ScaleToFit.FILL);
            mBitmapShader.setLocalMatrix(mScaleMatrix);
        }
    }

    void updateThumbnailPaintFilter() {
        if (mInvisible) {
            return;
        }
        int mul = (int) ((1.0f - mDimAlpha) * mThumbnailAlpha * 255);
        int add = (int) ((1.0f - mDimAlpha) * (1 - mThumbnailAlpha) * 255);
        if (mBitmapShader != null) {
            // mLightingColorFilter.setColorMultiply(Color.argb(255, mul, mul, mul));
            // mLightingColorFilter.setColorAdd(Color.argb(0, add, add, add));
            mDrawPaint.setColorFilter(mLightingColorFilter);
            mDrawPaint.setColor(0xffffffff);
        } else {
            int grey = mul + add;
            mDrawPaint.setColorFilter(null);
            mDrawPaint.setColor(Color.argb(255, grey, grey, grey));
        }
        invalidate();
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        mThumbnailAlpha = 1.0f;
        // mThumbnailAlpha = mConfig.taskViewThumbnailAlpha;
        updateThumbnailPaintFilter();
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    protected void onDraw(Canvas canvas) {
        // if (mInvisible) {
        //     return;
        // }
        if (null != mBitmap) {
            canvas.drawBitmap(mBitmap, 0f, 0f, null);
        }
        // Draw the thumbnail with the rounded corners
        // canvas.drawRoundRect(0f, 0f, getWidth(), getHeight(),
        //         // mConfig.taskViewRoundedCornerRadiusPx, mConfig.taskViewRoundedCornerRadiusPx,
        //         100f, 100f,
        //         mDrawPaint);
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (changed) {
            mLayoutRect.set(0, 0, getWidth(), getHeight());
            updateThumbnailScale();
        }
    }

}
