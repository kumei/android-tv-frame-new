# Download

```java
// 必须使用
implementation 'com.xgimi.tvwidget:ui:1.1.0'
// 依赖添加(可选，用不到的可以不加)
implementation rootProject.ext.dependencies["appcompat-v7"] // 使用 root.gradle
implementation rootProject.ext.dependencies["kotlin"]
implementation rootProject.ext.dependencies["common"]
implementation 'com.android.support:appcompat-v7:28.0.0' // 不使用 root.gradle
implementation 'org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.3.21'
implementation 'com.xgimi.tvwidget:commom:1.2.0' 
```



# TODO列表

* Leanback 优化策略
* 



# V1.0.6

* 添加 BaseWindowManagerView 悬浮窗的封装

* 添加handler，采用弱引用封装，防止内存泄露

 - 食用方式：
```
class Test {
    private val mCallback = Handler.Callback { msg ->
        when (msg.what) {
            1 -> {
            }
            else -> {
            }
        } //TODO
        true
    }
    private var mHandler: Handler? = WeakRefHandler(mCallback)

    fun test() {
        mHandler!!.sendEmptyMessage(1)
    }

    private fun onDestroy() {
        mHandler?.removeCallbacksAndMessages(null)
        mHandler = null
    }
}
```

