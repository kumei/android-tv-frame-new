# 相关文档

* [动画库文档](README_ANIM.md)

* [基础库文档](README_COMMON.md)
* [UI库文档](README_UI.md)



# 中央仓库使用

自建库需要如何加，打开 build.gradle

```groovy
apply plugin: 'com.android.library'
apply plugin: 'maven' // 加入这句

android {
	... ...
}

dependencies {
	... ...
}

//上传系统设置 presenter的 aar到中央仓库.
def MAVEN_LOCAL_PATH = 'http://172.21.16.247:8081/nexus/content/repositories/XgimiApi'
def ARTIFACT_ID = 'anim' // 一般为别名
def VERSION_NAME = '1.0.0' // 版本号
def GROUP_ID = 'com.xgimi.tvwidget' // 统一使用包名
def ACCOUNT = 'admin'
def PASSWORD = 'admin123'
// 最后会组装成这样 com.xgimi.tvwidget:anim:1.0.0，其它工厂这样调用 implementation 'com.xgimi.tvwidget:anim:1.0.0'
//脚本：将Lib打成aar包上传至maven私有库
uploadArchives {
    repositories {
        mavenDeployer {
            repository(url: MAVEN_LOCAL_PATH) {
                authentication(userName: ACCOUNT, password: PASSWORD)
            }
            pom.project {
                groupId GROUP_ID
                artifactId ARTIFACT_ID
                version VERSION_NAME
                packaging 'aar'
            }
        }
    }
}
```



引用库的时候，在项目的 build.gradle 的 android->repositories 里面加入 maven

```groovy
android {
    ... ...
	repositories {
    	flatDir {
        	dirs 'libs'
    	}
    	maven {
        	url 'http://172.21.16.247:8081/nexus/content/repositories/XgimiApi'
    	}
	}
}
```



