package com.open.common.joor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReflectTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testReflect() {
        String result = Reflect.on(String.class).call("valueOf", 1).get();
        assertEquals(result, "1");

        result = Reflect.on("java.lang.String").call("valueOf", 2).get();
        assertEquals(result, "2");
    }

}