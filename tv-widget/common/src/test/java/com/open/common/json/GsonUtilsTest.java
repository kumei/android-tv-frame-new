package com.open.common.json;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertSame;

public class GsonUtilsTest {

    GsonUtils mGsonUtils;
    String mTestJsonStr = "{\"message\":\"你是傻逼\",\"code\":250}";
    TestData mTestData = new TestData();

    @Before
    public void setUp() throws Exception {
        mTestData = new TestData();
        mTestData.setCode(250);
        mTestData.setMessage("你是傻逼");
    }

    @After
    public void tearDown() throws Exception {
    }


    @Test
    public void gsonString() {
        String jsonStr = mGsonUtils.gsonString(mTestData);
        System.out.println(jsonStr);
        assertEquals(jsonStr, mTestJsonStr);
    }

    @Test
    public void gsonToBean() {
        TestData testData = mGsonUtils.gsonToBean(mTestJsonStr, TestData.class);
        System.out.println(testData);
        assertEquals(testData.getMessage(), mTestData.getMessage());
        assertEquals(testData.getCode(), mTestData.getCode());
    }

    @Test
    public void gsonToList() {
    }

    @Test
    public void gsonToListMaps() {
    }

    @Test
    public void gsonToMaps() {
    }

    class TestData {
        String message;
        int code;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public int getCode() {
            return code;
        }

        public void setCode(int code) {
            this.code = code;
        }

        @Override
        public String toString() {
            return "TestData{" +
                    "message='" + message + '\'' +
                    ", code=" + code +
                    '}';
        }
    }
}