package com.open.common;

import android.content.Context;
import android.graphics.Color;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

public class ColorHelperTest {

    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void colorToString() {
        String colorStr = ColorHelper.colorToString(Color.RED);
        assertEquals(colorStr, "#FFFF0000");
    }
}