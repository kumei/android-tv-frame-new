package com.open.common.log

import android.support.test.runner.AndroidJUnit4
import org.junit.After
import org.junit.Before

import org.junit.Assert.*
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class XgimiLogUtilTest {

    @Before
    fun setUp() {
    }

    @After
    fun tearDown() {
    }

    @Test
    fun testXgimiLogUtil() {
        XgimiLogUtil.initLogUtil(true, "com.open.common")
        // android.util.Log mock
        XgimiLogUtil.e("XgimiLogUtilTest", "测试LOG函数是否正常");
    }

}