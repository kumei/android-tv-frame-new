package com.open.common.joor;

import android.annotation.SuppressLint;

/**
 * description: 反射读取环境变量.
 *
 * @author Sorgs.
 * Created date: 2019/5/30.
 */
public final class SystemProperties {

    /**
     * 反射获取环境变量
     *
     * @param key key
     * @param def 默认值
     * @return 获取的环境变量
     */
    @SuppressLint("PrivateApi")
    public static String getSystemProperties(String key, String def) {
        String value = def;
        /*try {
             Class<?> clazz = Class.forName("android.os.SystemProperties");
             Method method = clazz.getDeclaredMethod("get", String.class, String.class);
             value = method.invoke(clazz, key, def).toString();

        } catch (Exception e) {
            e.printStackTrace();
        }*/
        Object object = Reflect.on("android.os.SystemProperties").get();
        return Reflect.on(object).call("get", key, def).get();
    }

    @SuppressLint("PrivateApi")
    public static void setSystemProperties(String key, String value) {
       /* try {
            Class<?> clazz = Class.forName("android.os.SystemProperties");
            Method method = clazz.getDeclaredMethod("set", String.class, String.class);
            method.invoke(clazz, key, value);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
        Object object = Reflect.on("android.os.SystemProperties").get();
        Reflect.on(object).call("set", key, value);
    }
}
