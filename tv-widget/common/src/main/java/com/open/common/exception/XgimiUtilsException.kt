package com.open.common.exception


/**
 * @author rain.tang
 * @date 2019/3/5
 * description : 工具包异常类，用于抛出工具包内相关的异常信息
 */
class XgimiUtilsException : Exception {
    /**
     * 单异常信息构造函数
     * @param message 异常信息
     */
    constructor(message: String) : super(message)

    /**
     * 无参构造函数
     *
     */
    constructor() : super()

    /**
     * 异常信息加异常构造函数
     * @param detailMessage 异常信息
     * @param throwable 异常
     */
    constructor(detailMessage: String, throwable: Throwable) : super(detailMessage, throwable)

    /**
     * 单异常构造函数
     * @param throwbale 异常
     */
    constructor(throwable: Throwable) : super(throwable)
}