package com.open.common.json;

import android.content.Context;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * 关于 操作String相关的常用函数
 */
public class StringUtils {

    /**
     * 将 Raw 下的资源文件读取出来转换成 StringBuilder 类型
     *
     * @param context
     * @param id
     * @return StringBuilder
     */
    public static StringBuilder readRawToString(Context context, int id) {
        StringBuilder sb = new StringBuilder();
        try {
            InputStream is = context.getResources().openRawResource(id);
            InputStreamReader in = new InputStreamReader(is);
            BufferedReader bufferedReader = new BufferedReader(in);
            String line = null;
            while (null != (line = bufferedReader.readLine())) {
                sb.append(line);
            }
            bufferedReader.close();
            in.close();
            is.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sb;
    }

    public static <T> T readRawToBean(Context context, int id, Class<T> cls) {
        StringBuilder sb = readRawToString(context, id);
        return GsonUtils.gsonToBean(sb.toString(), cls);
    }

}
