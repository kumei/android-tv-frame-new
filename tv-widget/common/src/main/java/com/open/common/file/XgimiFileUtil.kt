package com.open.common.file

import com.open.common.exception.XgimiUtilsException
import com.open.common.exception.XgimiUtilsExceptionConstant
import com.open.common.log.XgimiLogUtil
import java.io.*
import java.nio.charset.Charset

/**
 * @author rain.tang
 * @date 2019/3/5
 * description : 文件相关操作工具类
 */
object XgimiFileUtil {
    private const val TAG = "XgimiFileUtil"
    /**
     * 将指定内容写入到文本文件中
     *
     * @param content 要写入的内容
     * @param dirPath 要写入的文件路径
     * @param filePath 需要写入的文件路径
     */
    fun writeContentToFile(content: String, dirPath: String, filePath: String) {
        //生成文件夹之后，再生成文件，不然会出错
        makeFilePath(dirPath, filePath)
        val file = File(filePath)
        var outputStreamWriter: OutputStreamWriter? = null
        if (!file.parentFile.exists()) {
            //文件的父路径不存在
            if (!file.parentFile.mkdirs()) {
                //创建文件的父路径失败，直接抛出异常
                throw XgimiUtilsException(XgimiUtilsExceptionConstant.MKDIR_IS_ERROR)
            }
            try {
                outputStreamWriter = OutputStreamWriter(FileOutputStream(filePath), Charset.forName("UTF-8"))
                outputStreamWriter.write(content)
                outputStreamWriter.close()
            } catch (e: Exception) {
                throw XgimiUtilsException(e)
            } finally {
                if (null != outputStreamWriter) {
                    try {
                        outputStreamWriter.close()
                    } catch (e: IOException) {
                        //关闭句柄异常,抛出打印
                        XgimiLogUtil.e("$TAG writeContentToFile final exception : ", e.toString())
                    }

                }
            }

        }
    }

    /**
     * 生成文件及文件路径
     *
     * @param dirPath  文件路径
     * @param filePath 文件名
     */
    fun makeFilePath(dirPath: String, filePath: String) {
        val dirFile = File(dirPath)
        val file = File(filePath)
        if (!dirFile.exists()) {
            if (!dirFile.mkdirs()) {
                //文件夹不存在且创建失败抛出相关异常
                throw XgimiUtilsException(XgimiUtilsExceptionConstant.MKDIR_IS_ERROR)
            }
        }
        if (!file.exists()) {
            try {
                if (!file.createNewFile()) {
                    //文件不存在且创建失败抛出相关异常
                    throw  XgimiUtilsException(XgimiUtilsExceptionConstant.CREATE_NEW_FILE_IS_ERROR)
                }
            } catch (e: Exception) {
                throw XgimiUtilsException(e.toString())
            }
        }
    }

    /**
     * 读取文件内容
     * @param filePath 文件绝对路径
     * @return 读取到的文件内容
     */
    fun readFileToString(filePath: String): String {
        val file = File(filePath)
        if (!file.exists()) {
            return ""
        }
        val sb = StringBuilder("")
        val fileInputStream = FileInputStream(filePath)
        val buffer = ByteArray(1024)
        var len = fileInputStream.read(buffer)
        //读取文件内容
        while (len > 0) {
            sb.append(String(buffer, 0, len))
            //继续将数据放到buffer中
            len = fileInputStream.read(buffer)
        }
        //关闭输入流
        fileInputStream.close()
        return sb.toString()
    }
}