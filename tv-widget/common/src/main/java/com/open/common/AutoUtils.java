package com.open.common;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.util.TypedValue;
import android.view.Display;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

/**
 * 适配库，网上转载 https://www.jianshu.com/p/ac9878cb0f6e
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 17-07-01 下午7:53
 * @Copyright: 2017 www.andriodtvdev.com Inc. All rights reserved.
 * @Other https://gitee.com/kumei/Android_tv_libs
 */
public class AutoUtils {

    private static final String KEY_DESIGN_WIDTH = "design_width";
    private static final String KEY_DESIGN_HEIGHT = "design_height";
    public static double textPixelsRate;
    private static int displayWidth;
    private static int displayHeight;
    private static int designWidth = 1920;
    private static int designHeight = 1080;
    private static boolean inited;

    public static void initSize(Context context, boolean hasStatusBar) {
        getMetaData(context);
        setSize(context, hasStatusBar, designWidth, designHeight);
    }

    /**
     * 获取XML中的 设计图分辨率配置.
     * <meta-data android:name="design_width" android:value="1920">
     * </meta-data>
     * <meta-data android:name="design_height" android:value="1080">
     * </meta-data>
     */
    private static void getMetaData(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo;
        try {
            applicationInfo = packageManager.getApplicationInfo(context
                    .getPackageName(), PackageManager.GET_META_DATA);
            if (applicationInfo != null && applicationInfo.metaData != null) {
                designWidth = (int) applicationInfo.metaData.get(KEY_DESIGN_WIDTH);
                designHeight = (int) applicationInfo.metaData.get(KEY_DESIGN_HEIGHT);
            }
        } catch (PackageManager.NameNotFoundException e) {
            throw new RuntimeException(
                    "请在AndroidManifest设置" + KEY_DESIGN_WIDTH + " 与 " + KEY_DESIGN_HEIGHT + "  的设计图分辨率.", e);
        }
    }

    /**
     * @param context
     * @param hasStatusBar
     * @param designWidth
     * @param designHeight
     */
    public static void setSize(Context context, boolean hasStatusBar, int designWidth, int designHeight) {
        if (context == null || designWidth < 1 || designHeight < 1) {
            return;
        }
        WindowManager mWManager = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = mWManager.getDefaultDisplay();
        int width = display.getWidth();
        int height = display.getHeight();
        if (hasStatusBar) {
            height -= getStatusBarHeight(context);
        }
        AutoUtils.displayWidth = width;
        AutoUtils.displayHeight = height;
        AutoUtils.designWidth = designWidth;
        AutoUtils.designHeight = designHeight;
        double displayDiagonal = Math.sqrt(Math.pow(AutoUtils.displayWidth, 2) + Math.pow(AutoUtils.displayHeight, 2));
        double designDiagonal = Math.sqrt(Math.pow(AutoUtils.designWidth, 2) + Math.pow(AutoUtils.designHeight, 2));
        AutoUtils.textPixelsRate = displayDiagonal / designDiagonal;
        inited = true;
    }

    public static int getStatusBarHeight(Context context) {
        int result = 0;
        try {
            int resourceId = context.getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                result = context.getResources().getDimensionPixelSize(resourceId);
            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        return result;
    }

    public static void initSize(Context context) {
        getMetaData(context);
        setSize(context, false, designWidth, designHeight);
    }

    public static boolean isInited() {
        return inited;
    }

    public static void auto(Activity act) {
        if (act == null || displayWidth < 1 || displayHeight < 1) {
            return;
        }
        View view = act.getWindow().getDecorView();
        auto(view);
    }

    public static void auto(View view) {
        if (view == null || displayWidth < 1 || displayHeight < 1) {
            return;
        }
        AutoUtils.autoTextSize(view);
        AutoUtils.autoSize(view);
        AutoUtils.autoPadding(view);
        AutoUtils.autoMargin(view);
        if (view instanceof ViewGroup) {
            auto((ViewGroup) view);
        }
    }

    private static void auto(ViewGroup viewGroup) {
        int count = viewGroup.getChildCount();
        for (int i = 0; i < count; i++) {
            View child = viewGroup.getChildAt(i);
            if (child != null) {
                auto(child);
            }
        }
    }

    public static void autoMargin(View view) {
        if (!(view.getLayoutParams() instanceof ViewGroup.MarginLayoutParams)) {
            return;
        }
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        if (lp == null) {
            return;
        }
        lp.leftMargin = getDisplayWidthValue(lp.leftMargin);
        lp.topMargin = getDisplayHeightValue(lp.topMargin);
        lp.rightMargin = getDisplayWidthValue(lp.rightMargin);
        lp.bottomMargin = getDisplayHeightValue(lp.bottomMargin);
    }

    public static void autoPadding(View view) {
        int l = view.getPaddingLeft();
        int t = view.getPaddingTop();
        int r = view.getPaddingRight();
        int b = view.getPaddingBottom();
        l = getDisplayWidthValue(l);
        t = getDisplayHeightValue(t);
        r = getDisplayWidthValue(r);
        b = getDisplayHeightValue(b);
        view.setPadding(l, t, r, b);
    }

    public static void autoSize(View view) {
        ViewGroup.LayoutParams lp = view.getLayoutParams();

        if (lp == null) {
            return;
        }

        if (lp.width > 0) {
            lp.width = getDisplayWidthValue(lp.width);
        }

        if (lp.height > 0) {
            lp.height = getDisplayHeightValue(lp.height);
        }
    }

    public static void autoTextSize(View view) {
        if (view instanceof TextView) {
            double designPixels = ((TextView) view).getTextSize();
            double displayPixels = textPixelsRate * designPixels;
            ((TextView) view).setIncludeFontPadding(false);
            ((TextView) view).setTextSize(TypedValue.COMPLEX_UNIT_PX, (float) displayPixels);
        }
    }

    public static int getDisplayWidthValue(int designWidthValue) {
        if (designWidthValue < 2) {
            return designWidthValue;
        }
        return designWidthValue * displayWidth / designWidth;

    }

    public static int getDisplayHeightValue(int designHeightValue) {
        if (designHeightValue < 2) {
            return designHeightValue;
        }
        return designHeightValue * displayHeight / designHeight;
    }

}
