package com.xgimi.common;

/**
 * 颜色工具类
 *
 * @author hailong.qiu
 * @date 2019.03.03
 */
public class ColorHelper {

    /**
     * 将 color 颜色值转换为十六进制字符串
     *
     * @param color 颜色值
     * @return 转换后的字符串
     */
    public static String colorToString(int color) {
        return String.format("#%08X", color);
    }

}
