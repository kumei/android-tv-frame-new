package com.xgimi.common;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.os.MessageQueue;

import java.util.LinkedList;

public class DeferredHandler {

    LinkedList<Runnable> mQueue = new LinkedList<>();
    /**
     * https://www.cnblogs.com/JohnTsai/p/5259869.html
     * MessageQueue：顾名思义，消息队列。内部存储着一组消息。
     * 对外提供了插入和删除的操作。
     * MessageQueue内部是以单链表的数据结构来存储消息列表的。
     */
    private MessageQueue mMessageQueue = Looper.myQueue();

    private Impl mHandler = new Impl();

    public DeferredHandler() {
    }

    /**
     * 闲时处理方式.
     */
    public void postIdle(final Runnable runnable) {
        post(new IdleRunnable(runnable));
    }

    /**
     * 立即分发方式
     */
    public void post(Runnable runnable) {
        synchronized (mQueue) {
            mQueue.add(runnable);
            if (mQueue.size() == 1) {
                scheduleNextLocked();
            }
        }
    }

    void scheduleNextLocked() {
        if (mQueue.size() > 0) {
            Runnable peek = mQueue.getFirst();
            if (peek instanceof IdleRunnable) {
                //  空闲
                mMessageQueue.addIdleHandler(mHandler);
            } else {
                mHandler.sendEmptyMessage(1);
            }
        }
    }

    /**
     * 无差别执行任务，执行消息队列中的每一个任务.
     */
    public void flush() {
        LinkedList<Runnable> queue = new LinkedList<>();
        synchronized (mQueue) {
            queue.addAll(mQueue);
            mQueue.clear();
        }
        for (Runnable r : queue) {
            r.run();
        }
    }

    public void cancelAll() {
        synchronized (mQueue) {
            mQueue.clear();
        }
    }

    class Impl extends Handler implements MessageQueue.IdleHandler {

        /**
         * 当线程空闲时，就会去调用queueIdle()函数
         */
        @Override
        public boolean queueIdle() {
            handleMessage(null);
            return false;
        }

        @Override
        public void handleMessage(Message msg) {
            Runnable r;
            synchronized (mQueue) {
                if (mQueue.size() == 0) {
                    return;
                }
                r = mQueue.removeFirst();
            }
            r.run();
            synchronized (mQueue) {
                scheduleNextLocked();
            }
        }
    }

    private class IdleRunnable implements Runnable {
        Runnable mRunnable;

        IdleRunnable(Runnable r) {
            mRunnable = r;
        }

        @Override
        public void run() {
            mRunnable.run();
        }
    }

}
