package com.xgimi.common;

import android.os.Handler;
import android.os.HandlerThread;

import java.lang.ref.WeakReference;

public class LauncherMode {

    /**
     * 耗时的操作，用这个sWorker
     */
    static final HandlerThread sWorkerThread = new HandlerThread("launcher-loader");
    static final Handler sWorker = new Handler(sWorkerThread.getLooper());

    static {
        sWorkerThread.start();
    }

    WeakReference<Callbacks> mCallbacks;
    private DeferredHandler mHandler = new DeferredHandler();

    /**
     * Launcher 回调，更新界面
     */
    public interface Callbacks {
    }

}
