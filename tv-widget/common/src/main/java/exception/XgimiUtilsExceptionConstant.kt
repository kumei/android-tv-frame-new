package exception

/**
 * @author rain.tang
 * @date 2019/3/5
 * description : 工具包异常信息常量类
 */
class XgimiUtilsExceptionConstant {
    companion object {
        /**
         * 文件夹创建失败
         */
        const val MKDIR_IS_ERROR = "Mkdir Is Error"
        /**
         * 文件创建失败
         */
        const val CREATE_NEW_FILE_IS_ERROR = "CreateNewFile Is Error"
        /**
         * 删除文件失败
         */
        const val DELETE_FILE_IS_ERROR = "DeleteFile Is Error"
        /**
         * 该条目不是文件
         */
        const val ITEM_IS_NOT_FILE = "It's Is Not File"
        /**
         * 未初始化该模块
         */
        const val NOT_INIT_MODULE = "Please Init This Module"
    }

}