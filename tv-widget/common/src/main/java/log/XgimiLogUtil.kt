package log

import android.util.Log

/**
 * setprop log.tag.PACKAGE_NAME DEBUG
 *
 * @author rain.tang
 * @date 2019/3/5
 * description : 日志信息类，用于打印各种等级日志 , error等级日志将在release版本后保留输出
 */
object XgimiLogUtil {

    private var IS_DEBUG = false
    private var PACKAGE_NAME = ""
    /**
     * 初始化日志工具类
     * @param isDebug 是否为调试模式
     * @param packageName 打印统一的项目名TAG
     */
    fun initLogUtil(isDebug: Boolean, packageName: String) {
        IS_DEBUG = isDebug
        PACKAGE_NAME = packageName
    }

    /**
     * debug日志等级
     * @param tag 打印tag
     * @param content 日志内容
     */
    fun d(tag: String, content: String) {
        if (isDebug()) {
            Log.d(String.format("%s_%s", PACKAGE_NAME, tag), content)
        }
    }

    /**
     * verbose日志等级
     * @param tag 打印tag
     * @param content 日志内容
     */
    fun v(tag: String, content: String) {
        if (isDebug()) {
            Log.v(String.format("%s_%s", PACKAGE_NAME, tag), content)
        }
    }

    /**
     * error日志等级debug/release版本都会保留打印
     * @param tag 打印tag
     * @param content 日志内容
     */
    fun e(tag: String, content: String) {
        Log.e(String.format("%s_%s", PACKAGE_NAME, tag), content)
    }

    /**
     * info日志等级
     * @param tag 打印tag
     * @param content 日志内容
     */
    fun i(tag: String, content: String) {
        if (isDebug()) {
            Log.i(String.format("%s_%s", PACKAGE_NAME, tag), content)
        }
    }

    /**
     * warning日志等级
     * @param tag 打印tag
     * @param content 日志内容
     */
    fun w(tag: String, content: String) {
        if (isDebug()) {
            Log.w(String.format("%s_%s", PACKAGE_NAME, tag), content)
        }
    }

    fun isDebug(): Boolean {
        return IS_DEBUG || Log.isLoggable(PACKAGE_NAME, Log.DEBUG)
    }

}