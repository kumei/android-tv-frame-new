package file

import android.content.Context
import android.content.SharedPreferences

/**
 * @author rain.tang
 * @date 2019/3/5
 * description : SharePreferences工具类,用于保存简单的配置信息
 */
class XgimiSharedPreferencesUtils {
    private var mFileName = "sp_date"
    private var mSharedPreferences: SharedPreferences? = null
    private val mString = "String"
    private val mInteger = "Integer"
    private val mBoolean = "Boolean"
    private val mFloat = "Float"
    private val mLong = "Long"

    fun initSharePreferences(context: Context, spFileName: String) {
        mFileName = spFileName
        mSharedPreferences = context.getSharedPreferences(mFileName, Context.MODE_PRIVATE)
    }

    /**
     * 保存数据的方法，我们需要拿到保存数据的具体类型，然后根据类型调用不同的保存方法
     *
     * @param key    key
     * @param value value
     * @param isApply 是否提交，简单数据可以直接传入true进行save -> apply，较复杂数据为保证性能，建议一次性全部save后手动触发apply
     */
    fun save(key: String, value: Any, isApply: Boolean) {
        val type = value.javaClass.simpleName
        val editor = mSharedPreferences!!.edit()
        when {
            mString == type -> editor.putString(key, value as String)
            mInteger == type -> editor.putInt(key, value as Int)
            mBoolean == type -> editor.putBoolean(key, value as Boolean)
            mFloat == type -> editor.putFloat(key, value as Float)
            mLong == type -> editor.putLong(key, value as Long)
        }
        if (isApply) {
            editor.apply()
        }
    }

    fun apply() {
        val editor = mSharedPreferences!!.edit()
        editor.apply()
    }


    /**
     *
     * 获取保存数据的方法，我们根据默认值得到保存的数据的具体类型，然后调用相对于的方法获取值
     *
     * @param key           key
     * @param defaultObject defaultValue
     * @return 相同于defaultObject的数据类型
     */
    fun load(key: String, defaultObject: Any): Any? {
        val type = defaultObject.javaClass.simpleName
        return when {
            mString == type -> mSharedPreferences!!.getString(key, defaultObject as String)
            mInteger == type -> mSharedPreferences!!.getInt(key, defaultObject as Int)
            mBoolean == type -> mSharedPreferences!!.getBoolean(key, defaultObject as Boolean)
            mFloat == type -> mSharedPreferences!!.getFloat(key, defaultObject as Float)
            mLong == type -> mSharedPreferences!!.getLong(key, defaultObject as Long)
            else -> null
        }

    }

    fun clear() {
        mSharedPreferences!!.edit().clear().apply()
    }

    fun remove(key: String) {
        mSharedPreferences!!.edit().remove(key).apply()
    }
}