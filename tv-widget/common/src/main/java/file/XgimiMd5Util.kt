package file

import exception.XgimiUtilsException
import exception.XgimiUtilsExceptionConstant
import java.io.File
import java.io.FileInputStream
import java.security.MessageDigest
import kotlin.experimental.and

/**
 * @author rain.tang
 * @date 2019/3/5
 * description : 文件MD5工具类
 */
class XgimiMd5Util {
    private val md5Method = "MD5"
    @Throws(XgimiUtilsException::class)
    fun getFileMd5(filePath: String): String? {
        val file = File(filePath)
        if (!file.isFile) {
            throw XgimiUtilsException(XgimiUtilsExceptionConstant.ITEM_IS_NOT_FILE)
        }
        val digest: MessageDigest
        val fileInputStream: FileInputStream
        val buffer = ByteArray(1024)
        var len: Int
        try {
            digest = MessageDigest.getInstance(md5Method)
            fileInputStream = FileInputStream(file)
            while ((fileInputStream.read().also { len = it }) != -1) {
                digest.update(buffer, 0, len)
            }
            fileInputStream.close()
        } catch (e: Exception) {
            throw XgimiUtilsException(e)
        }
        return bytesToHexString(digest.digest())
    }

    private fun bytesToHexString(src: ByteArray?): String? {
        val stringBuilder = StringBuilder()
        if (src == null || src.isEmpty()) {
            return null
        }
        for (aSrc in src) {
            val v = aSrc and 0xFF.toByte()
            val hv = Integer.toHexString(v.toInt())
            if (hv.length < 2) {
                stringBuilder.append(0)
            }
            stringBuilder.append(hv)
        }
        return stringBuilder.toString()
    }
}