# Download

```java
// 必须使用
implementation 'com.xgimi.tvwidget:commom:1.2.0'
// 依赖添加(可选，用不到的可以不加)
implementation rootProject.ext.dependencies["appcompat-v7"] // 使用 root.gradle
implementation rootProject.ext.dependencies["kotlin"]
implementation rootProject.ext.dependencies["gson"]
implementation 'com.android.support:appcompat-v7:28.0.0' // 不使用 root.gradle
implementation 'org.jetbrains.kotlin:kotlin-stdlib-jdk7:1.3.21'
implementation 'com.google.code.gson:gson:2.8.5'
```



# v1.0.7

* 增加 GsonUtils工具类
* 增加 SystemProperties 类

* 增加LOG日志打印

Log日志打印工具类使用:
在Application中初始化log日志工具类
param:是否开启打印，应用包名(便于筛选)
XgimiLogUtil.initLogUtil(true,"com.xgimi.test");



* 增加Toast工具类

Toast工具类使用:
在Application中初始化Toast上下文
XgimiToastUtil.initToastUtils(this);