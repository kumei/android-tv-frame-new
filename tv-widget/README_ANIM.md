
# Download
```java
// 必须使用
implementation 'com.xgimi.tvwidget:anim:1.2.1'
// 依赖添加
implementation 'com.xgimi.tvwidget:commom:1.2.0' 
implementation rootProject.ext.dependencies["appcompat-v7"] // 使用 root.gradle
implementation 'com.android.support:appcompat-v7:28.0.0' // 不使用 root.gradle
```



# DEMO参考:

[AnimActivity](http://gitlab.xgimi-dev.com/system-app/tv-widget/blob/master/app/src/main/java/com/xgimi/tvwidget/AnimActivity.java)



# TODO列表:

* 支持 XML动画(补齐  play ater befer with，sequentially 等)
* 修复生命周期问题(只有两个生效的问题)
* 全局可以取消/开始所有动画(用于界面滚动优化)(参考 Glide.with(context).resumeRequests(); Glide.with(context).pauseRequests())
* RecyclerView Anim 支持
* SVG 动画支持???
* litho 支持???

# v1.2.2
* 支持 width height 改变的动画


# v1.2.1([文档](http://gitlab.xgimi-dev.com/system-app/tv-widget/raw/master/doc/AnimDoc_v1.2.1.zip))
* BUG：多个不同的时间无效的问题
* 支持 TypeEvaluator
* play ater befer with 支持设置属性
* 局部延时支持
* 支持XML动画



# v1.2.0
* 修改 包名问题



# v1.1.1([文档](http://gitlab.xgimi-dev.com/system-app/tv-widget/raw/master/doc/AnimDoc_v1.1.1.zip))
* 修改支持生命周期, 添加with,into方法.
* 全局配置插值器,时间支持



# v1.0.2

* 修改支持多个view
* 修改支持多个view同时动画
* 支持生命周期
Activity或Fragment的onStart而resume，onStop而pause，onDestroy而clear，
还有 View 的 onAttachedToWindow，onDetachedFromWindow(clear)
从而节约流量和内存，并且防止内存泄露


