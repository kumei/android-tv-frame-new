package com.open.ui.layout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

/**
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 19-02-15 下午7:53
 * @Copyright: 2017 www.andriodtvdev.com Inc. All rights reserved.
 * @Other https://gitee.com/kumei/Android_tv_libs
 */
public class UIButton extends Button {

    public UIButton(Context context) {
        this(context, null);
    }

    public UIButton(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UIButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

}
