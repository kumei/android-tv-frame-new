package com.open.ui.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.ImageView;

import com.open.ui.R;

@SuppressLint("AppCompatCustomView")
public class TVUIRadiusImageView extends ImageView {

    private static final int DEFAULT_BORDER_COLOR = Color.GRAY;

    private int mBorderColor;

    public TVUIRadiusImageView(Context context) {
        this(context, null, R.attr.TVUIRadiusImageViewStyle);
    }

    public TVUIRadiusImageView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TVUIRadiusImageView, defStyleAttr, 0);
        mBorderColor = array.getColor(R.styleable.TVUIRadiusImageView_tvui_border_color, DEFAULT_BORDER_COLOR);
        array.recycle();
        //
        setBackgroundColor(mBorderColor);
    }

    public TVUIRadiusImageView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.TVUIRadiusImageViewStyle);
    }

}
