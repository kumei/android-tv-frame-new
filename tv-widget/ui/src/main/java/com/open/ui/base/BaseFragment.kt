package com.open.ui.base

import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

/**
 * description: Fragment基类
 *
 * @author Sorgs.
 * Created date: 2018/4/8.
 */
abstract class BaseFragment : Fragment() {
    /**
     * Tag
     */
    protected var TAG = javaClass.simpleName
    /**
     * 上下文
     */
    protected open var mContext: Context? = null
    protected open var mActivity: Activity? = null

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        mContext = context
        mActivity = activity
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        return inflater.inflate(initLayoutId(), null)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(savedInstanceState)
        initData()
        initListener()
    }

    protected open fun initView(savedInstanceState: Bundle?) {

    }

    protected open fun initData() {
    }

    protected open fun initListener() {

    }


    override fun onDetach() {
        super.onDetach()
        mContext = null
    }

    /**
     * 初始化布局
     *
     * @return 布局id
     */
    protected abstract fun initLayoutId(): Int

}
