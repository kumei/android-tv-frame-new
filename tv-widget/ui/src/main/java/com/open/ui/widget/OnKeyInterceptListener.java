package com.open.ui.widget;

import android.view.KeyEvent;

public interface OnKeyInterceptListener {
    boolean onInterceptKeyEvent(KeyEvent event);
}
