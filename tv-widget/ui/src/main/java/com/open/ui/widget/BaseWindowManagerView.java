package com.open.ui.widget;

import android.content.Context;
import android.graphics.PixelFormat;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.open.common.AutoUtils;
import com.open.ui.R;

import java.lang.ref.WeakReference;

/**
 * 悬浮窗口封装，避免重复创建
 */
public class BaseWindowManagerView implements OnKeyInterceptListener {

    private RootFrameLayout mRootLayout;
    private WeakReference<Context> mContextWeakReference;
    private WindowManager mWindowManager;
    private WindowManager.LayoutParams mParams;
    private boolean mIsShow = false;

    public BaseWindowManagerView(Context context, int type, int animStyle) {
        mContextWeakReference = new WeakReference<>(context);
        mWindowManager = (WindowManager) mContextWeakReference.get().getSystemService(Context.WINDOW_SERVICE);
        mParams = new WindowManager.LayoutParams();
        // 类型
        mParams.type = type;
        mParams.height = WindowManager.LayoutParams.MATCH_PARENT;
        mParams.width = WindowManager.LayoutParams.MATCH_PARENT;
        // 设置flag
        int flags = WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM;
        mParams.flags = flags;
        // 不设置这个弹出框的透明遮罩显示为黑色
        mParams.format = PixelFormat.TRANSLUCENT;
        mParams.windowAnimations = animStyle;
        // 添加主Root布局
        mRootLayout = new RootFrameLayout(getContext());
        mRootLayout.setOnKeyInterceptListener(this);
    }

    public BaseWindowManagerView(Context context, int type) {
        this(context, type, R.style.BaseWinMenuShowStyle);
    }

    public BaseWindowManagerView(Context context) {
        this(context,
                WindowManager.LayoutParams.TYPE_APPLICATION_SUB_PANEL | WindowManager.LayoutParams.TYPE_SYSTEM_OVERLAY);
    }

    public RootFrameLayout getRootLayout() {
        return mRootLayout;
    }

    public Context getContext() {
        return mContextWeakReference.get();
    }

    public void setContentView(View view) {
        if (view != null) {
            if (mRootLayout.getParent() != null) {
                mWindowManager.removeView(mRootLayout);
            }
            mWindowManager.addView(mRootLayout, mParams);
            mRootLayout.removeAllViews();
            RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.MATCH_PARENT,
                    RelativeLayout.LayoutParams.MATCH_PARENT);
            mRootLayout.addView(view, lp);
        }
        mIsShow = true;
    }

    @Override
    public boolean onInterceptKeyEvent(KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_UP && event.getKeyCode() == KeyEvent.KEYCODE_BACK) {
            onBackPressed();
            return true;
        }
        return false;
    }

    public boolean isShow() {
        return mIsShow;
    }

    public void onBackPressed() {
        try {
            if (mIsShow) {
                mIsShow = false;
                mWindowManager.removeView(mRootLayout);
                mRootLayout.removeAllViews();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onDestroy() {
        onBackPressed();
    }

    public static class RootFrameLayout extends RelativeLayout {

        public RootFrameLayout(Context context) {
            this(context, null);
        }

        public RootFrameLayout(Context context, AttributeSet attrs) {
            this(context, attrs, 0);
        }

        public RootFrameLayout(Context context, AttributeSet attrs, int defStyleAttr) {
            super(context, attrs, defStyleAttr);
            AutoUtils.auto(this);
        }

        private OnKeyInterceptListener mOnKeyInterceptListener;

        public void setOnKeyInterceptListener(OnKeyInterceptListener onKeyInterceptListener) {
            this.mOnKeyInterceptListener = onKeyInterceptListener;
        }

        @Override
        public boolean dispatchKeyEvent(KeyEvent event) {
            if (mOnKeyInterceptListener != null && mOnKeyInterceptListener.onInterceptKeyEvent(event)) {
                return true;
            }
            return super.dispatchKeyEvent(event);
        }

        @Override
        public RelativeLayout.LayoutParams generateLayoutParams(AttributeSet attrs) {
            return new RelativeLayout.LayoutParams(getContext(), attrs);
        }

    }

}