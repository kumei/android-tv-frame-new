package com.open.ui.base

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity

/**
 * description: Activity基本.
 *
 * @author Sorgs.
 * Created date: 2019/4/8.
 */
abstract class BaseActivity : AppCompatActivity() {
    /**
     * Tag
     */
    protected var TAG = javaClass.simpleName
    /**
     * 上下文
     */
    protected open lateinit var mContext: Context

    @SuppressLint("InlinedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val layoutResID = initLayoutId()
        //如果initView返回0,则不会调用setContentView()
        if (layoutResID != 0) {
            setContentView(layoutResID)
        }
        mContext = this
        initView(savedInstanceState)
        initData()
        initListener()
    }

    /**
     * 添加布局文件
     *
     * @return 布局文件
     */
    protected abstract fun initLayoutId(): Int

    protected open fun initView(savedInstanceState: Bundle?) {
    }

    protected open fun initData() {

    }

    protected open fun initListener() {

    }
}
