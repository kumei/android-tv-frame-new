package com.open.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.open.common.AutoUtils;
import com.open.ui.R;

/**
 * 参考 QMUICommonListItemView
 */
public class TVUICommonItemView extends RelativeLayout {

    private ImageView mImageView; // 左侧图标
    private ViewGroup mAccessoryView; // 右侧容器，比如进度，开关，箭头，自定义等
    private TextView mTextView;
    private TextView mDetailTextView;

    public TVUICommonItemView(Context context) {
        this(context, null, R.attr.TVUICommonItemViewStyle);
    }

    public TVUICommonItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        LayoutInflater.from(context).inflate(R.layout.tvui_common_list_item, this, true);
        /* 初始化获取属性值 */
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TVUICommonItemView, defStyleAttr, 0);
        final int testColor = array.getColor(R.styleable.TVUICommonItemView_tvui_commonList_titleColor,
                Color.BLACK);
        array.recycle();
        /* 适配控件 */
        AutoUtils.auto(this);
        /* 初始化所有view */
        initAllView();
        /* 初始化控件的属性 */
        // mTextView.setText(initTitleText);
        // mDetailTextView.setText(initDetailText);
        // mTextView.setTextColor(getAttrColor(context, R.attr.item_detail_color));
        setBackgroundColor(testColor);
    }

    private void initAllView() {
        mImageView = (ImageView) findViewById(R.id.group_list_item_iv);
        mAccessoryView = findViewById(R.id.group_list_item_accessory_view);
        mTextView = findViewById(R.id.group_list_item_tv);
        mDetailTextView = findViewById(R.id.group_list_item_detail_tv);
    }

    public static int getAttrColor(Context context, int attrRes) {
        TypedValue typedValue = new TypedValue();
        context.getTheme().resolveAttribute(attrRes, typedValue, true);
        return typedValue.data;
    }

    public TVUICommonItemView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.TVUICommonItemViewStyle);
    }

    public void setImageDrawable(Drawable drawable) {
        if (drawable == null) {
            mImageView.setVisibility(View.GONE);
        } else {
            mImageView.setImageDrawable(drawable);
            mImageView.setVisibility(View.VISIBLE);
        }
    }

    public void setText(CharSequence text) {
        mTextView.setText(text);
        if (TextUtils.isEmpty(text)) {
            mTextView.setVisibility(View.GONE);
        } else {
            mTextView.setVisibility(View.VISIBLE);
        }
    }

    public void setDetailText(CharSequence text) {
        mDetailTextView.setText(text);
        if (TextUtils.isEmpty(text)) {
            mDetailTextView.setVisibility(View.GONE);
        } else {
            mDetailTextView.setVisibility(View.VISIBLE);
        }
    }

}
