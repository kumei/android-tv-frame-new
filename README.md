#Android-TV-开发框架
Android TV开发框架 重构后的最新版本.

Android TV 开源社区 https://gitee.com/kumei/Android_tv_libs

Android TV 开源论坛 www.androidtvdev.com

Android TV 文章专题 https://www.jianshu.com/c/3f0ab61a1322

Android TV  QQ群1：522186932   QQ群2：468357191



本项目包含一整套开发 TV 的套件

* 基础库-常用的一些函数
* UI
* 通过动画库
* 通用网络请求库(解耦)
* 通过网络加载库



目录结构

## TVUI 库

- UIButton 圆角，边框颜色，背景等等支持
- UILayout 圆角，阴影，边框，倒影，扫光 等等支持
- UIGroupListView 各种样式的列表支持
- UITextView 优化TextView
- UITheme 支持全局配置，全局样式，控件颜色.
- ... ...


- leanback 不修改源码的前提下解耦，加载更多，移动边框，滚动插值器 等 支持
- 丰富的工具类：设备，网络，任务加载管理，适配库 等等





## v1.0.0

- [x] 支持插值器
- [ ] 增加一些函数方便调用
- [ ] 加载更多
- [ ] 支持焦点移动框
- [ ] 边界支持


## 感谢支持项目

[![在这里插入图片描述](https://images.gitee.com/uploads/images/2020/0521/011611_7e2f4f87_111902.jpeg)](http://www.cdnbye.com/)

