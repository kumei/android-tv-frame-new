package android.support.v17.leanback.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;

public class VerticalMoreGridView extends VerticalGridView {
    public VerticalMoreGridView(Context context) {
        this(context, null);
    }

    public VerticalMoreGridView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VerticalMoreGridView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initLoadMoreEvents();
    }

    /**
     * 加载更多 start start start
     */
    private OnLoadListener mOnLoadListener;
    private MoreState mMoreState = MoreState.STATE_IDLE;

    /**
     * 加载更多支持.
     */
    private void initLoadMoreEvents() {
        addOnScrollListener(new OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if (newState == RecyclerView.SCROLL_STATE_IDLE &&
                        mMoreState == MoreState.STATE_IDLE) {
                    if (mLayoutManager.getOrientation() == RecyclerView.VERTICAL && isFocusOnRightmostColumn()) {
                        if (null != mOnLoadListener) {
                            mMoreState = MoreState.STATE_LOADING_MORE;
                            mOnLoadListener.onLoadMore();
                        }
                    }
                }
            }
        });
    }

    /**
     * 用于垂直的加载更多判断.
     */
    public boolean isFocusOnRightmostColumn() {
        if (mLayoutManager != null && getFocusedChild() != null) {
            int position = mLayoutManager.getPosition(getFocusedChild());
            int rowCount = mLayoutManager.getItemCount() / mLayoutManager.getNumRows();
            int rowNum = position / mLayoutManager.getNumRows();
            return rowNum == rowCount - 1;
        }
        return false;
    }

    /**
     * 加载更多结束后的回调.
     * 调用后刷新状态.
     */
    @Override
    public void onLoadComplete() {
        mMoreState = MoreState.STATE_IDLE;
    }

    /**
     * 恢复刷新加载更多.
     */
    @Override
    public void onMoreReset() {
        mMoreState = MoreState.STATE_IDLE;
    }

    /**
     * 没有更多加载，防止再次加载更多.
     * 1. 比如 第一次获取 24 个，返回只有 data_num(20) < 24，可以调用此函数
     * 2. N次加载更多后，后续返回的 data_num(20) < 24，调用此函数，结束后续的加载更多.
     */
    @Override
    public void onMoreOver() {
        mMoreState = MoreState.STATE_MORE_OVER;
    }

    /**
     * 按键加载更多，满足条件就调用 onLoadMore，继续加载更多的操作.
     */
    @Override
    public void setOnLoadMoreListener(OnLoadListener loadListener) {
        mOnLoadListener = loadListener;
    }

    @Override
    public void setLoadMoreState(MoreState state) {
        mMoreState = state;
    }

}
