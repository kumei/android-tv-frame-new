package com.open.ui.widget;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Color;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.open.common.device.AutoUtils;
import com.open.ui.R;

/**
 * 参考 QMUICommonListItemView
 */
public class TVUICommonItemView extends RelativeLayout {

    private ImageView mImageView; // 左侧图标
    private ViewGroup mAccessoryView; // 右侧容器，比如进度，开关，箭头，自定义等

    public TVUICommonItemView(Context context) {
        this(context, null);
    }

    public TVUICommonItemView(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.TVUICommonItemViewStyle);
    }

    public TVUICommonItemView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
//        LayoutInflater.from(context).inflate(R.layout.tvui_common_list_item, this, true);
//        //
        TypedArray array = context.obtainStyledAttributes(attrs, R.styleable.TVUICommonItemView, defStyleAttr, 0);
        int color = array.getColor(R.styleable.TVUICommonItemView_tvui_commonList_titleColor, Color.BLACK);
        setBackgroundColor(color);
        array.recycle();
//        // 适配
//        AutoUtils.auto(this);
//        //
//        mImageView = (ImageView) findViewById(R.id.group_list_item_iv);
//        mAccessoryView = findViewById(R.id.group_list_item_accessoryView);
    }

}
