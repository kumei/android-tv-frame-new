package com.open.ui.layout;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.widget.Button;

import com.open.ui.R;

/**
 *
 */
public class UIButton extends Button {

    UIRoundDrawable mUIRoundDrawable;

    public UIButton(Context context) {
        this(context, null);
    }

    public UIButton(Context context, AttributeSet attrs) {
        this(context, attrs, R.attr.TVUIRoundButtonStyle);
    }

    public UIButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mUIRoundDrawable = UIRoundDrawable.build(context, attrs, defStyleAttr);
        // 设置背景
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            setBackground(mUIRoundDrawable);
        } else {
            setBackgroundDrawable(mUIRoundDrawable);
        }
        // 设置 padding
        setPadding(0, 0, 0, 0);
    }

    @Override
    public void setBackgroundColor(int color) {
        mUIRoundDrawable.setBgData(ColorStateList.valueOf(color));
    }

}
