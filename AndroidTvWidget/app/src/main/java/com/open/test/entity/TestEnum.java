package com.open.test.entity;

/**
 * 枚举常量测试
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 2019/3/29
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public enum TestEnum {
    TEST_A_TYPE(0);

    private final int _type;

    TestEnum(int type) {
        _type = type;
    }

    public int value() {
        return _type;
    }
}
