package com.open.test.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v17.leanback.widget.ArrayObjectAdapter;
import android.support.v17.leanback.widget.FocusHighlightHandler;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ItemBridgeAdapter;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.LoadMoreInterface;
import android.support.v17.leanback.widget.VerticalGridView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;

import com.open.common.device.AutoUtils;
import com.open.test.presenter.AppItemPresenter;
import com.open.test.presenter.TestPresenterSelector;
import com.open.tv.test.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Leanback demo界面
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 17-07-01 下午7:53
 * @Copyright: 2017 www.andriodtvdev.com Inc. All rights reserved.
 * @Other https://gitee.com/kumei/Android_tv_libs
 */
public class LeanbackActivity extends Activity {

    View loadPb;
    VerticalGridView hGridView;
    Button moreBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_leanback);
        loadPb = findViewById(R.id.load_pb);
        moreBtn = findViewById(R.id.more_btn);
        moreBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                hGridView.onMoreReset();
                hGridView.getAdapter().notifyDataSetChanged();
            }
        });
        initVerticalGridView();
        LinearLayoutManager lm;
    }

    private void initVerticalGridView() {
        hGridView = findViewById(R.id.hrecy_view);
        AutoUtils.auto(hGridView);
        hGridView.setFocusOutAllowed(true, true);
        hGridView.setFocusOutSideAllowed(true, true);
//        hGridView.setInterpolator(PathInterpolatorCompat.create(0.18f, 0.00f, 0.00f, 1.00f));
//        hGridView.setInterpolator(new LinearInterpolator());
//        hGridView.setInterpolator(new DecelerateInterpolator());
        final ArrayObjectAdapter rowsAdapter = new ArrayObjectAdapter(new TestPresenterSelector());
        ItemBridgeAdapter itemBridgeAdapter = new ItemBridgeAdapter(rowsAdapter);
        hGridView.setAdapter(itemBridgeAdapter);
        //
        for (int i = 0; i < 30; i++) {
           ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new AppItemPresenter(i));
           List<String> itemList = new ArrayList<>();
           for (int j = 0; j < 30; j++) {
               itemList.add("item" + j);
           }
           listRowAdapter.addAll(0, itemList);
           HeaderItem settingHeader = new HeaderItem(i, "测试" + i);
           ListRow listRow = new ListRow(i, settingHeader, listRowAdapter);
           rowsAdapter.add(listRow);
            // rowsAdapter.add("item" + i);
        }
        //
        itemBridgeAdapter.notifyDataSetChanged();
//        hGridView.setOnLoadMoreListener(new LoadMoreInterface.OnLoadListener() {
//            @Override
//            public void onLoadMore() {
//                loadPb.setVisibility(View.VISIBLE);
//                new Thread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            Thread.sleep(1000);
//                        } catch (InterruptedException e) {
//                            e.printStackTrace();
//                        }
//                        LeanbackActivity.this.runOnUiThread(new Runnable() {
//                            @Override
//                            public void run() {
//                                for (int i = 0; i < 10; i++) {
//                                    ArrayObjectAdapter listRowAdapter = new ArrayObjectAdapter(new AppItemPresenter(i));
//                                    List<String> itemList = new ArrayList<>();
//                                    for (int j = 0; j < 30; j++) {
//                                        itemList.add("item" + j);
//                                    }
//                                    listRowAdapter.addAll(0, itemList);
//                                    HeaderItem settingHeader = new HeaderItem(i, "测试" + i);
//                                    ListRow listRow = new ListRow(i, settingHeader, listRowAdapter);
//                                    rowsAdapter.add(listRow);
//                                }
//                                hGridView.onLoadComplete();
//                                hGridView.onMoreReset(); // 恢复加载更多.
//                                hGridView.onMoreOver(); // 没有加载更多.
//                                loadPb.setVisibility(View.GONE);
//                            }
//                        });
//                    }
//                }).start();
//            }
//        });
//        hGridView.setItemViewCacheSize(5);
//         hGridView.setRecycledViewPool(new RecyclerView.RecycledViewPool());

        ((ItemBridgeAdapter)hGridView.getAdapter()).setFocusHighlight(new FocusHighlightHandler() {
            @Override
            public void onItemFocused(View view, boolean hasFocus) {
                view.animate().scaleX(hasFocus ? 1.21f : 1.0f).scaleY(hasFocus ? 1.21f : 1.0f).setDuration(hasFocus ? 100 : 400).start();
            }

            @Override
            public void onInitializeView(View view) {
            }
        });
    }

}
