package com.open.test.activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.open.tv.test.R;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class RecyclerActivity extends Activity {

    RecyclerView mRecyclerView;
    TextView mLogTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recyclerview);
        ButterKnife.bind(this);
        mLogTv = findViewById(R.id.log_tv);
        mRecyclerView = findViewById(R.id.recycler_view);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        mRecyclerView.setAdapter(new RecyclerView.Adapter() {
            @NonNull
            @Override
            public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int type) {
                // View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item, viewGroup, false);
                View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_test,
                        parent, false);
                Log.d("hailong.qiu", "onCreateViewHolder type:" + type);
                mLogTv.setText("onCreateViewHolder type:" + type + "\n" + mLogTv.getText());
                return new NewViewHolder(view);
            }

            @Override
            public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                Button button = (Button) viewHolder.itemView;
                button.setText("Item" + i);
                Log.d("hailong.qiu", "onBindViewHolder index:" + i);
                mLogTv.setText("onBindViewHolder index:" + i + "\n" + mLogTv.getText());
            }

            @Override
            public int getItemCount() {
                return 100;
            }

        });
    }

    @OnClick({R.id.clear_log_btn, R.id.set_cache_size_btn, R.id.set_recycled_pool_btn, R.id.set_layout_space_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.clear_log_btn: // 清楚缓存信息.
                mLogTv.setText("Recler log info:");
                break;
            case R.id.set_cache_size_btn:
                mRecyclerView.setItemViewCacheSize(4);
                break;
            case R.id.set_recycled_pool_btn: // 设置 缓存池.
                mRecyclerView.setRecycledViewPool(new RecyclerView.RecycledViewPool() {
                    @Override
                    public int getRecycledViewCount(int viewType) {
                        return super.getRecycledViewCount(viewType);
                    }
                });
                break;
            case R.id.set_layout_space_btn: // 设置 LayoutSpace
                mRecyclerView.setLayoutManager(new LinearLayoutManager(this) {
                    @Override
                    protected int getExtraLayoutSpace(RecyclerView.State state) {
                        return 600;
                    }
                });
                break;
        }
    }

    class NewViewHolder extends RecyclerView.ViewHolder {
        public NewViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }

}
