package com.open.test;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;

public class LauncherModel extends BroadcastReceiver {

    /**
     * HandlerThread 是 Thread 的一个子类，
     * 这种类型的线程就能确保消息队列中
     * 的每个请求按照先进先出的顺序执行，
     * 除非消息队列被重置. (主线程的一个子线程)
     */
    static final HandlerThread sWorkerThread = new HandlerThread("launcher-loader");

    static {
        sWorkerThread.start();
    }

    static final Handler sWorker = new Handler(sWorkerThread.getLooper());

    @Override
    public void onReceive(Context context, Intent intent) {
    }

}

