package com.open.test.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.open.test.entity.TestEnum;
import com.open.tv.test.R;

/**
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 17-07-01 下午7:53
 * @Copyright: 2017 www.andriodtvdev.com Inc. All rights reserved.
 * @Other https://gitee.com/kumei/Android_tv_libs
 */
public class TestActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        ButterKnife.bind(this);

        ScrollView sv;
        RecyclerView rv;
        LinearLayoutManager lm;
        LinearLayout ly;
        FrameLayout fl;
        RelativeLayout relativeLayout;
        CardView cv;
        ConstraintLayout constraintLayout;
        // test 枚举常量
        int type = TestEnum.TEST_A_TYPE.value();
        TestEnum.TEST_A_TYPE.toString();
    }

    @OnClick({R.id.leanback_btn, R.id.recy_btn})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.recy_btn:
                startActivity(new Intent(this, RecyclerActivity.class));
                break;
            case R.id.leanback_btn:
                startActivity(new Intent(this, LeanbackActivity.class));
                break;
        }
    }

}
