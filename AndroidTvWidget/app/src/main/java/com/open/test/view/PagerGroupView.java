package com.open.test.view;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.widget.Scroller;

import java.lang.reflect.Field;

public class PagerGroupView extends ViewPager {

    public PagerGroupView(@NonNull Context context) {
        this(context, null);
    }

    public PagerGroupView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        // 替换 NewScroll
        try {
            Field localField = ViewPager.class.getDeclaredField("mScroller");
            localField.setAccessible(true);
                localField.set(this, new NewScroll(context));
            localField.setAccessible(false);
        } catch (NoSuchFieldException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean executeKeyEvent(@NonNull KeyEvent event) {
        return false;
    }

    class NewScroll extends Scroller {
        public NewScroll(Context context) {
            super(context);
        }

        @Override
        public void startScroll(int startX, int startY, int dx, int dy) {
            super.startScroll(startX, startY, dx, dy*3);
        }
    }

}
