package com.open.test.presenter;

import android.support.v17.leanback.widget.Presenter;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.open.common.device.AutoUtils;
import com.open.test.view.ItemTextView;
import com.open.tv.test.R;

/**
 * item presenter.
 */
public class AppItemPresenter extends Presenter {

    int mLayoutType = 0;

    public AppItemPresenter() {

    }

    public AppItemPresenter(int type) {
        mLayoutType = type % 3;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        int layoutID = R.layout.item2;
       if (mLayoutType == 1) {
           layoutID = R.layout.item;
       } else if (mLayoutType == 2) {
           layoutID = R.layout.item3;
       }
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutID,
                parent, false);
        AutoUtils.auto(view);
        return new TestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, Object item) {
        if (null != viewHolder.view.getParent() && viewHolder.view.getParent() instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) viewHolder.view.getParent();
            vg.setClipToPadding(false);
            vg.setClipChildren(false);
        }
        TestViewHolder tvh = (TestViewHolder) viewHolder;
//        if (mLayoutType != 1) {
            tvh.tv.setText((String) item);
//        } else {
//            ItemTextView tv = viewHolder.view.findViewById(R.id.text);
////            tv.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL);
//            tv.setText((CharSequence) item);
//        }
    }

    @Override
    public void onUnbindViewHolder(ViewHolder viewHolder) {
        TestViewHolder tvh = (TestViewHolder) viewHolder;
        tvh.tv.setText(null);
    }

    class TestViewHolder extends ViewHolder {
        TextView tv;
        public TestViewHolder(View view) {
            super(view);
//            if (mLayoutType != 1) {
                tv = view.findViewById(R.id.text);
//            }
        }
    }
}
