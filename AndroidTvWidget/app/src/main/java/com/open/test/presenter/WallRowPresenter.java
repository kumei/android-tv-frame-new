package com.open.test.presenter;

import android.support.v17.leanback.widget.RowPresenter;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.open.common.device.AutoUtils;
import com.open.test.view.FocusGridLayout;
import com.open.test.view.FocusOnTopGridLayout;
import com.open.tv.test.R;

/**
 * 拼图墙 RowPresenter.
 */
public class WallRowPresenter extends RowPresenter {

    @Override
    protected ViewHolder createRowViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_wall_view,
                parent, false);
        AutoUtils.auto(view);
        view.setPadding(AutoUtils.getDisplayWidthValue(90), 0, AutoUtils.getDisplayWidthValue(90), 0);
        return new ViewHolder(view);
    }

    @Override
    protected void onBindRowViewHolder(ViewHolder vh, Object item) {
        super.onBindRowViewHolder(vh, item);
        //
        FocusGridLayout focusGridLayout = (FocusGridLayout) vh.view;
//        FocusGridLayout.LayoutParams lg = (FocusGridLayout.LayoutParams) focusGridLayout.getLayoutParams();
        focusGridLayout.setOnChildFocusListener(new FocusOnTopGridLayout.OnChildFocusListener() {
            @Override
            public void onChildFocus(View child, View focused) {
                ViewCompat.setScaleX(child, 1.0f);
                ViewCompat.setScaleY(child, 1.0f);
                ViewCompat.setScaleX(focused, 1.2f);
                ViewCompat.setScaleY(focused, 1.2f);
            }
        });
        // test focus events.
        int childCount = focusGridLayout.getChildCount();
        for (int i = 0; i < childCount; i++) {
            View childView = focusGridLayout.getChildAt(i);
            childView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (!hasFocus) {
                        ViewCompat.setScaleX(v, 1.0f);
                        ViewCompat.setScaleY(v, 1.0f);
                    } else {
                        ViewCompat.setScaleX(v, 1.2f);
                        ViewCompat.setScaleY(v, 1.2f);
                    }
                }
            });
        }
    }

    @Override
    protected void onUnbindRowViewHolder(ViewHolder vh) {
        super.onUnbindRowViewHolder(vh);
    }

}
