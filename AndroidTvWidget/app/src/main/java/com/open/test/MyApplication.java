package com.open.test;

import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;

/**
 * Application类
 *
 * @Author: hailong.qiu 356752238@qq.com
 * @Maintainer: hailong.qiu 356752238@qq.com
 * @Date: 2019/3/17
 * @Copyright: 2019 www.andriodtvdev.com Inc. All rights reserved.
 */
public class MyApplication extends Application {

    private static final String TAG = "hailong.qiu";

    @Override
    public void onCreate() {
        super.onCreate();
    }

    /**
     * 系统要求应用释放内存时，level 为级别
     *
     * @param level
     */
    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    /**
     * 系统低内存时调用
     */
    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    /**
     * 系统配置变化
     *
     * @param newConfig
     */
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    /**
     * 应用创建时
     *
     * @param base
     */
    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
    }

    /**
     * 应用结束时
     */
    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
