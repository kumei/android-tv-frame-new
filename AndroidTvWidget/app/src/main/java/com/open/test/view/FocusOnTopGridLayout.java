package com.open.test.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.GridLayout;

public class FocusOnTopGridLayout extends GridLayout {

    OnChildFocusListener mOnChildFocusListener;
    GridLayoutRecycler mGridLayoutRecycler;

    public FocusOnTopGridLayout(Context context) {
        this(context, null);
    }

    public FocusOnTopGridLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FocusOnTopGridLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //
        setChildrenDrawingOrderEnabled(true);
        setFocusable(true);
        setFocusableInTouchMode(true);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        View focusView = super.focusSearch(focused, direction);
        if ((focusView == null) && (direction == View.FOCUS_RIGHT)) {
            // 到达最右边，焦点下移.
            FocusVerticalGridView.onExecutorServiceSubmit(KeyEvent.KEYCODE_DPAD_DOWN);
        } else if ((focusView == null) && (direction == View.FOCUS_LEFT)) {
            // 到达最左边，焦点下移.
            FocusVerticalGridView.onExecutorServiceSubmit(KeyEvent.KEYCODE_DPAD_UP);
        }
        return focusView;
    }

    /**
     * 绘制顺序处理.
     */
    @Override
    protected int getChildDrawingOrder(int childCount, int index) {
        View focusView = getFocusedChild();
        if (focusView == null) {
            return index;
        }
        int focusIndex = indexOfChild(focusView);
        if (index < focusIndex) {
            return index;
        } else if (index < childCount - 1) {
            return focusIndex + childCount - 1 - index;
        } else {
            return focusIndex;
        }
    }

    @Override
    public void requestChildFocus(View child, View focused) {
        super.requestChildFocus(child, focused);
        if (null != mOnChildFocusListener) {
            mOnChildFocusListener.onChildFocus(child, focused);
        }
        invalidate();
    }

    public void putRecycler(Recycler recycler, int index) {
        if (null != mGridLayoutRecycler) {
            mGridLayoutRecycler.putRecycler(recycler, index);
        }
    }

    public Recycler getRecycler(int index) {
        if (null != mGridLayoutRecycler) {
            mGridLayoutRecycler.getRecycler(index);
        }
        return null;
    }

    /**
     * 设置缓存
     */
    public void setRecyclerPool(GridLayoutRecycler gridLayoutRecycler) {
        mGridLayoutRecycler = gridLayoutRecycler;
    }

    public void setOnChildFocusListener(OnChildFocusListener cb) {
        mOnChildFocusListener = cb;
    }

    public static abstract interface OnChildFocusListener {
        public abstract void onChildFocus(View child, View focused);
    }

}
