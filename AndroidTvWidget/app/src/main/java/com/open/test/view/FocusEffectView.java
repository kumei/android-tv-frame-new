package com.open.test.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

import com.open.tv.test.R;

/**
 * 优化阴影+边框，避免过度绘制.
 */
public class FocusEffectView extends ScanLightView {

    Drawable mEffectDrawable = getResources().getDrawable(R.drawable.img_focus);
    Rect mEffectRect = new Rect();

    public FocusEffectView(Context context) {
        this(context, null);
    }

    public FocusEffectView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FocusEffectView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        mEffectDrawable.getPadding(mEffectRect);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.save();
        canvas.translate(-mEffectRect.left, -mEffectRect.top);
        mEffectDrawable.draw(canvas);
        canvas.restore();
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        mEffectDrawable.setBounds(0,
                0,
                getWidth() + mEffectRect.left + mEffectRect.right,
                getHeight() + mEffectRect.top + mEffectRect.bottom);
    }

    public void setFocusDrawable(int paramInt) {
        mEffectDrawable = getResources().getDrawable(paramInt);
        mEffectDrawable.getPadding(mEffectRect);
    }

}
