package com.open.test.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.View;

/**
 * 优化TextView，无跑马灯并且短文本的使用.
 */
public class ItemTextView extends View {

    private static TextPaint mTextPaint = null;
    private static Rect mTextRect = new Rect();
    private static Rect mDrawRect = new Rect();
    private String mText = "测试效果";
    private int mGravity = Gravity.CENTER;

    public ItemTextView(Context context) {
        this(context, null);
    }

    public ItemTextView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ItemTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (null == mTextPaint) {
            mTextPaint = new TextPaint();
            mTextPaint.density = getResources().getDisplayMetrics().density;
            mTextPaint.setTextSize(38);
            mTextPaint.setColor(Color.BLACK);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (!TextUtils.isEmpty(mText)) {
            canvas.save();
            mTextPaint.getTextBounds(mText, 0, mText.length(), mTextRect);
            mDrawRect.set(0, 0, getWidth(), getHeight());
            canvas.clipRect(mDrawRect);
            if (mGravity != Gravity.CENTER) {
                canvas.drawText(mText, 5.0F, getHeight() / 2 + mTextRect.height() / 2 + getPaddingTop(), mTextPaint);
            } else if (mTextRect.width() >= getWidth()) {
                canvas.drawText(mText, 5.0F, getHeight() / 2 + mTextRect.height() / 2 + getPaddingTop(), mTextPaint);
            } else {
                canvas.drawText(mText,
                        (getWidth() - getPaddingLeft() - getPaddingRight() - mTextRect.width()) / 2,
                        getHeight() / 2 + mTextRect.height() / 2 + getPaddingTop(),
                        mTextPaint);
            }
            canvas.restore();
        }
    }

    @Override
    public void setFocusable(int focusable) {
        super.setFocusable(focusable);
        invalidate();
    }

    public void setGravity(int gravity) {
        mGravity = gravity;
    }

    public void setText(CharSequence paramCharSequence) {
        setText(paramCharSequence.toString());
    }

    public void setText(String text) {
        mText = text;
        invalidate();
    }

}
