package com.open.test.presenter;

import android.graphics.Color;
import android.support.v17.leanback.widget.HeaderItem;
import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.Row;
import android.support.v17.leanback.widget.RowHeaderPresenter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * 标题头.
 */
public class NewRowHeaderPresenter extends RowHeaderPresenter {

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent) {
        return new HeadViewHolder(new TextView(parent.getContext()));
    }

    @Override
    public void onBindViewHolder(Presenter.ViewHolder viewHolder, Object item) {
        HeaderItem headerItem = item == null ? null : ((Row) item).getHeaderItem();
        if (null != headerItem) {
            TextView tv = (TextView) viewHolder.view;
            tv.setTextSize(30);
            tv.setPadding(10, 10, 10, 10);
            tv.setTextColor(Color.RED);
            tv.setText(headerItem.getName());
        }
    }

    @Override
    public void onUnbindViewHolder(Presenter.ViewHolder viewHolder) {
        TextView tv = (TextView) viewHolder.view;
        tv.setText(null);
    }

    public static class HeadViewHolder extends ViewHolder {
        public HeadViewHolder(View view) {
            super(view);
        }
    }

}
