package com.open.test.view;

public interface IRecycler {
    Object getMapClass(Class<?> _class);
    void putMapClass(Class<?> _class, Object object);
}
