package com.open.test.activity;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.TextView;

@SuppressLint("AppCompatCustomView")
public class TestTextView extends TextView {

    public TestTextView(Context context) {
        super(context);
    }

    public TestTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public TestTextView(Context context,  AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }


    @Override
    public void requestLayout() {
        super.requestLayout();
    }

//    @Override
//    public View focusSearch(int direction) {
//        isFocus = true;
//        if (direction == View.FOCUS_LEFT) {
//            isFocus = false;
//        }
//        return super.focusSearch(direction);
//    }

//        @Override
//    public boolean isFocused() {
//        return true;
////        return super.isFocused();
//    }

}
