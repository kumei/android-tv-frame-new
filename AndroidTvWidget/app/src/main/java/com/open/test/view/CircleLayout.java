package com.open.test.view;

import android.content.Context;
import android.support.v17.leanback.widget.BaseCardView;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;

/**
 * compile 'com.android.support:cardview-v7:21.0.+'
 */
public class CircleLayout extends CardView {
    public CircleLayout(Context context) {
        this(context, null);
    }

    public CircleLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CircleLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        float size = Math.min((getHeight() - getPaddingTop() - getPaddingBottom()) / 2.0F, (getWidth() - getPaddingLeft() - getPaddingRight()) / 2.0F);
        if (size < 0.0F) {
            size = 0.0F;
        }
        setRadius(size);
    }
}
