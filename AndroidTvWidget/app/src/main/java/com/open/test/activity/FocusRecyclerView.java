package com.open.test.activity;

import android.content.Context;
import android.graphics.Rect;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;

import java.util.ArrayList;

public class FocusRecyclerView extends RecyclerView {
    public FocusRecyclerView(@NonNull Context context) {
        this(context, null);
    }

    public FocusRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FocusRecyclerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
//        setDescendantFocusability(FOCUS_AFTER_DESCENDANTS); // 先分发给Child View进行处理，如果所有的Child View都没有处理，则自己再处理
//        setOverScrollMode(View.OVER_SCROLL_NEVER);
    }

    @Override
    protected boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
        if (mFocudView != null) {
            boolean result = mFocudView.requestFocus(direction, previouslyFocusedRect);
            return result;
        }
        return super.onRequestFocusInDescendants(direction, previouslyFocusedRect);
    }

    View mFocudView;

    @Override
    public void addFocusables(ArrayList<View> views, int direction, int focusableMode) {
        if (hasFocus()) {
            mFocudView = getFocusedChild();
        } else {
            if (isFocusable()) {
                views.add(this);
                return;
            }
        }
        super.addFocusables(views, direction, focusableMode);
    }

    @Override
    public View focusSearch(View focused, int direction) {
        View nextView = super.focusSearch(focused, direction);
        if (null == nextView && null != mOnFocusSearchLister) {
            nextView = mOnFocusSearchLister.onFocusSearchFailed(focused, direction);
        }
        return nextView;
    }

    OnFocusSearchLister mOnFocusSearchLister;

    public void setOnFocusSearchLister(OnFocusSearchLister cb) {
        mOnFocusSearchLister = cb;
    }

    public interface OnFocusSearchLister {
        public View onFocusSearchFailed(@NonNull View focused, int direction);
    }

}
