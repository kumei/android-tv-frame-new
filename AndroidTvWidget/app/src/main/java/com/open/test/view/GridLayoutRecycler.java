package com.open.test.view;

import android.util.SparseArray;
import android.view.View;

import java.util.ArrayList;

public class GridLayoutRecycler {

    SparseArray<ArrayList<Recycler>> mSparseList = new SparseArray();

    public Recycler getRecycler(int index) {
        if (null != mSparseList.get(index) && mSparseList.get(index).size() > 0) {
            return mSparseList.get(index).remove(mSparseList.get(index).size() - 1);
        }
        return null;
    }

    /**
     * 添加缓存view.
     * @param view
     * @param index
     */
    public void putRecycler(Recycler view, int index) {
        if (null == mSparseList.get(index)) {
            mSparseList.put(index, new ArrayList());
        }
        mSparseList.get(index).add(view);
    }

}
