package com.open.test.presenter;

import android.graphics.Rect;
import android.support.v17.leanback.widget.HorizontalGridView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Leanback Decoration item 间距
 *
 * @Author: hailong.qiu hailong.qiu@xgimi.com
 * @Maintainer: hailong.qiu hailong.qiu@xgimi.com
 * @Date: 18-11-01
 * @Copyright: 2017 https://www.xgimi.com/ Inc. All rights reserved.
 */
public class SpaceItemDecoration extends RecyclerView.ItemDecoration {
    private int mRightSpace;
    private int mLeftSpace;
    private int mTopSpace;
    private int mBottomSpace;

    public SpaceItemDecoration(int leftSpace, int topSpace, int rightSpace, int bottomSpace) {
        this.mRightSpace = rightSpace;
        this.mLeftSpace = leftSpace;
        this.mTopSpace = topSpace;
        this.mBottomSpace = bottomSpace;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.right = mLeftSpace;
        outRect.left = mRightSpace;
        outRect.top = mTopSpace;
        outRect.bottom = mBottomSpace;
    }

}
