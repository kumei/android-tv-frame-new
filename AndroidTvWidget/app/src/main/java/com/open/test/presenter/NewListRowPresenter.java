package com.open.test.presenter;

import android.support.v17.leanback.widget.FocusHighlightHandler;
import android.support.v17.leanback.widget.HorizontalGridView;
import android.support.v17.leanback.widget.ListRowPresenter;
import android.support.v17.leanback.widget.OnChildSelectedListener;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.RowPresenter;
import android.support.v4.view.ViewCompat;
import android.support.v4.view.animation.PathInterpolatorCompat;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.LinearInterpolator;

import com.open.common.device.AutoUtils;

public class NewListRowPresenter extends ListRowPresenter {

    @Override
    protected RowPresenter.ViewHolder createRowViewHolder(ViewGroup parent) {
        ListRowPresenter.ViewHolder vh = (ViewHolder) super.createRowViewHolder(parent);
        HorizontalGridView horizontalGridView = vh.getGridView();
        AutoUtils.auto(horizontalGridView);
        horizontalGridView.setFocusDrawingOrderEnabled(true);
        horizontalGridView.addItemDecoration(new SpaceItemDecoration(10, 10, 10, 10));
        //
        // 测试bug效果
//        horizontalGridView.setRowHeight(-2);
//        horizontalGridView.setFocusable(false);
//        horizontalGridView.setFocusableInTouchMode(false);
//        horizontalGridView.setHasFixedSize(Build.VERSION.SDK_INT <= Build.VERSION_CODES.LOLLIPOP_MR1);//6.0以下系统使用
//        horizontalGridView.setHasFixedSize(true);
        horizontalGridView.setClipChildren(false);
        horizontalGridView.setClipToPadding(false);
        horizontalGridView.setFocusOutAllowed(false, true);
//        horizontalGridView.setAnimateChildLayout(false);
//        horizontalGridView.setHorizontalMargin(90);
        horizontalGridView.setPadding(90, 0, 90, 0);
        //
//        horizontalGridView.setFocusScrollStrategy(HorizontalGridView.FOCUS_SCROLL_ITEM); // 去掉中间滚动和焦点记忆.
        /**
         * 优化布局.
         * true: 当前 View（注意此处为父容器）是 ViewGroup，且模式为 LAYOUT_MODE_OPTICAL_BOUNDS
         * ViewGroup 模式有两种：
         * LAYOUT_MODE_CLIP_BOUNDS：默认模式，表示边界未加工的
         * LAYOUT_MODE_OPTICAL_BOUNDS：大体含义是支持特效，如阴影、暖色、冷色
         */
        horizontalGridView.setLayoutMode(ViewGroup.LAYOUT_MODE_OPTICAL_BOUNDS);
//        horizontalGridView.setInterpolator(PathInterpolatorCompat.create(0.33f, 0.00f, 0.67f, 1.00f));
//        horizontalGridView.setInterpolator(new AccelerateDecelerateInterpolator());
        //
        setShadowEnabled(false); // 阴影开关
//        setSelectEffectEnabled(false);
        return vh;
    }

    @Override
    protected void initializeRowViewHolder(RowPresenter.ViewHolder holder) {
        super.initializeRowViewHolder(holder);
        final ViewHolder rowViewHolder = (ViewHolder) holder;
        rowViewHolder.getBridgeAdapter().setFocusHighlight(new FocusHighlightHandler() {
            @Override
            public void onItemFocused(View view, boolean hasFocus) {
                view.animate().scaleX(hasFocus ? 1.21f : 1.0f).scaleY(hasFocus ? 1.21f : 1.0f).setDuration(hasFocus ? 100 : 400).start();
            }

            @Override
            public void onInitializeView(View view) {
            }
        });
//        rowViewHolder.getBridgeAdapter().setWrapper(null);
        rowViewHolder.getGridView().setFocusDrawingOrderEnabled(true);
        rowViewHolder.getGridView().setOnChildSelectedListener(null);
    }

    @Override
    protected void onBindRowViewHolder(RowPresenter.ViewHolder holder, Object item) {
        super.onBindRowViewHolder(holder, item);
    }

}
