package com.open.test.view;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

public class FocusGridLayout extends FocusOnTopGridLayout {

    public FocusGridLayout(Context context) {
        this(context, null);
    }

    public FocusGridLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FocusGridLayout(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        setChildrenDrawingOrderEnabled(true);
        setFocusable(true);
        setFocusableInTouchMode(true);
        setDescendantFocusability(ViewGroup.FOCUS_AFTER_DESCENDANTS);
    }

    @Override
    public void addFocusables(ArrayList<View> views, int direction, int focusableMode) {
        if (hasFocus()) {
            super.addFocusables(views, direction, focusableMode);
        } else if (isFocusable()) {
            views.add(this);
        }
    }

    @Override
    protected boolean onRequestFocusInDescendants(int direction, Rect previouslyFocusedRect) {
        int i = -1;
        int j = getChildCount();
        Rect localObject1 = new Rect();
        Rect localObject2 = new Rect();
        int k;
        int m;

        if (direction != View.FOCUS_UP) {
            if (getChildCount() == 0) {
                return false;
            }
            getChildAt(0).getFocusedRect(localObject1);
            offsetDescendantRectToMyCoords(getChildAt(0), localObject1);
            k = 1;
            m = 0;
            if (k != j) {
                if (getChildAt(k).getVisibility() != View.VISIBLE) {
                    localObject2 = localObject1;
                }
                getChildAt(k).getFocusedRect(localObject2);
                offsetDescendantRectToMyCoords(getChildAt(k), localObject2);
                if (localObject2.bottom <= localObject1.bottom) {
                    localObject2 = localObject1;
                }
                m = k;
            }
        }

        int i1;
        int n;
        if (direction != View.FOCUS_FORWARD) {
            i = j;
            i1 = 1;
            n = 0;
        }

//            for (k = 0; k < j - 1; k++) {
//                View localView = getChildAt(k);
//                if ((localView.getVisibility() == View.VISIBLE) && (localView.requestFocus(direction, previouslyFocusedRect))) {
//                    return true;
//                }
//            }
//        getChildAt(2).requestFocus(direction, previouslyFocusedRect);
        return super.onRequestFocusInDescendants(direction, previouslyFocusedRect);
    }

}
