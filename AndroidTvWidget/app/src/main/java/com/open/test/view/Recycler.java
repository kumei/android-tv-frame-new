package com.open.test.view;

import android.view.KeyEvent;
import android.view.View;

import java.util.HashMap;
import java.util.Map;

public class Recycler implements IRecycler {

    private Map<Class, Object> mClassMap;
    public final View mView;

    public Recycler(View view) {
        mView = view;
    }

    @Override
    public Object getMapClass(Class<?> _class) {
        if (mClassMap == null) {
            return null;
        }
        return mClassMap.get(_class);
    }

    @Override
    public void putMapClass(Class<?> _class, Object object) {
        if (mClassMap == null) {
            mClassMap = new HashMap();
        }
        mClassMap.put(_class, object);
    }

}
