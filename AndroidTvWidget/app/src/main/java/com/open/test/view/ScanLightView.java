package com.open.test.view;

import android.animation.ValueAnimator;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

/**
 * 扫光效果View.
 * Mode.ADD 参考:https://blog.csdn.net/wuyou1336/article/details/54933221
 */
public class ScanLightView extends View implements ValueAnimator.AnimatorUpdateListener {

    Drawable mDrawable = null;
    ValueAnimator mValueAnimator = null;
    float mAnimValue = 0.0f;
    Paint mPaint = null;
    boolean mIsAnim = false;

    public ScanLightView(Context context) {
        this(context, null);
    }

    public ScanLightView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ScanLightView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        //
        mValueAnimator = new ValueAnimator();
        mValueAnimator.setDuration(1200);
        mValueAnimator.addUpdateListener(this);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.ADD));
        mPaint.setAlpha(120);
    }

    public void startAnim() {
        mValueAnimator.cancel();
        mValueAnimator.start();
        mIsAnim = true;
    }

    public void endAnim() {
        mValueAnimator.end();
    }

    public void loopAnim() {
        mValueAnimator.cancel();
        mValueAnimator.start();
        mValueAnimator.setRepeatCount(-1);
        mIsAnim = true;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        int w;
        int h;
        if (mDrawable instanceof BitmapDrawable) {
            canvas.save();
            canvas.clipRect(getPaddingLeft(),
                    getPaddingTop(),
                    getWidth() - getPaddingRight(),
                    getHeight() - getPaddingBottom());
            w = getWidth() - getPaddingLeft() - getPaddingRight();
            h = getHeight() - getPaddingTop() - getPaddingBottom();
            if ((w > 0) && (h > 0) && (mAnimValue > 0) && (mAnimValue < 1)) {
                float f1 = (float)(Math.sqrt(w * w + h * h) / Math.sqrt(187200.0D));
                if (mIsAnim) {
                    mValueAnimator.setDuration((long) (1200f * f1));
                    mIsAnim = false;
                }
                canvas.scale(f1, f1);
                if (w < h) {
                    float f2 = -mDrawable.getIntrinsicHeight() + getPaddingTop() + (h + mDrawable.getIntrinsicHeight()) * mAnimValue;
                    canvas.translate(
                            -mDrawable.getIntrinsicWidth() / 2 - w * (mDrawable.getIntrinsicHeight() / 2) / h + getPaddingLeft() + (w + w * mDrawable.getIntrinsicHeight() / h) * mAnimValue,
                            f2);
                }
            }
            canvas.drawBitmap(((BitmapDrawable)mDrawable).getBitmap(), 0.0F, 0.0F, mPaint);
            canvas.restore();
            canvas.restore();
        }
    }

    @Override
    public void onAnimationUpdate(ValueAnimator animation) {
        mAnimValue = ((Float)animation.getAnimatedValue()).floatValue();
        invalidate();
    }

}
