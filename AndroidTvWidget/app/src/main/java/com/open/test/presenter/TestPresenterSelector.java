package com.open.test.presenter;

import android.support.v17.leanback.widget.ListRow;
import android.support.v17.leanback.widget.Presenter;
import android.support.v17.leanback.widget.PresenterSelector;

/**
 * 选择器
 */
public class TestPresenterSelector extends PresenterSelector {

    NewListRowPresenter mListRowPresenter = new NewListRowPresenter();
    WallRowPresenter mWallRowPresenter = new WallRowPresenter();

    public TestPresenterSelector() {
    }

    @Override
    public Presenter[] getPresenters() {
        return new Presenter[]{mListRowPresenter, mWallRowPresenter};
    }

    @Override
    public Presenter getPresenter(Object item) {
        ListRow listRow = (ListRow) item;
        if (listRow.getId() == 2) {
           mWallRowPresenter.setHeaderPresenter(new NewRowHeaderPresenter());
           return mWallRowPresenter;
        }
       mListRowPresenter.setHeaderPresenter(new NewRowHeaderPresenter());
        return mListRowPresenter;
//        return new AppItemPresenter();
    }

}
