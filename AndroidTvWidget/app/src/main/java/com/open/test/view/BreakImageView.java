package com.open.test.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

@SuppressLint("AppCompatCustomView")
public class BreakImageView extends ImageView {

    public BreakImageView(Context context) {
        super(context);
    }

    public BreakImageView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public BreakImageView(Context paramContext, AttributeSet attrs, int defStyleAttr) {
        super(paramContext, attrs, defStyleAttr);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();
        if (getDrawable() != null) {
            setPivotY(getDrawable().getIntrinsicHeight());
            int w = getDrawable().getIntrinsicWidth();
            setPivotX(w / 2);
            if (!TextUtils.isEmpty((String)getTag())) {
                if ("left".equals(getTag())) {
                    setPivotX(0);
                } else if ("right".equals(getTag())) {
                    setPivotX(w);
                }
            }
        }
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        View localView = (View) getParent();
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        if (getDrawable() != null) {
            ((ViewGroup.MarginLayoutParams) getLayoutParams()).topMargin = (-(getDrawable().getIntrinsicHeight() - localView.getHeight()));
            setPivotY(getDrawable().getIntrinsicHeight());
            return;
        }
        setPivotY(1.2F * localView.getHeight());
        setPivotX(localView.getWidth() / 2);
    }

    @Override
    public void setImageDrawable(Drawable drawable) {
        if (null != drawable) {
            setPivotY(drawable.getIntrinsicHeight());
            int w = drawable.getIntrinsicWidth();
            setPivotX(w / 2);
            if (!TextUtils.isEmpty((String) getTag())) {
                if ("left".equals(getTag())) {
                    setPivotX(0);
                } else if ("right".equals(getTag())) {
                    setPivotX(w);
                }
            }
        }
        super.setImageDrawable(drawable);
//        getParent().requestLayout();
//        ((View)getParent()).invalidate();
    }

}
