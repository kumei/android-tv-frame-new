package com.open.common.util;

import android.os.Handler;
import android.os.HandlerThread;
import java.lang.ref.WeakReference;

public class LauncherMode {

    WeakReference<Callbacks> mCallbacks;

    /**
     * 耗时的操作，用这个sWorker
     */
    static final HandlerThread sWorkerThread = new HandlerThread("launcher-loader");
    static {
        sWorkerThread.start();
    }
    static final Handler sWorker = new Handler(sWorkerThread.getLooper());

    private DeferredHandler mHandler = new DeferredHandler();

    /**
     * Launcher 回调，更新界面
     */
    public interface Callbacks {
    }

}
